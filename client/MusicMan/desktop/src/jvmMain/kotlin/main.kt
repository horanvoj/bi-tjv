import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.window.WindowDraggableArea
import androidx.compose.ui.Alignment
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowPosition
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import cz.fit.cvut.horanvoj.musicman.App
import cz.fit.cvut.horanvoj.musicman.presentation.root.WindowHeader
import java.awt.Dimension

fun main() = application {
    val windowState = rememberWindowState(
        size = DpSize(800.dp, 800.dp),
        position = WindowPosition.Aligned(Alignment.Center)
    )
    Window(
        state = windowState,
        title = "MusicMan",
        onCloseRequest = ::exitApplication,
        undecorated = true,
    ) {
        window.minimumSize = Dimension(200, 200)
        Column {
            WindowDraggableArea {
                WindowHeader(
                    onCloseClick = ::exitApplication
                )
            }
            App()
        }
    }
}
