package cz.fit.cvut.horanvoj.musicman.presentation.common

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.platform.LocalLayoutDirection

actual fun Modifier.imePadding() = imePadding()
actual fun Modifier.statusBarsPadding() = statusBarsPadding()
actual fun Modifier.navigationBarsPadding() = navigationBarsPadding()
actual fun Modifier.imePadding(atLeast: PaddingValues) = composed {
    val direction = LocalLayoutDirection.current
    WindowInsets.ime.asPaddingValues().let { ime ->
        PaddingValues(
            start = ime.calculateStartPadding(direction).coerceAtLeast(atLeast.calculateStartPadding(direction)),
            end = ime.calculateEndPadding(direction).coerceAtLeast(atLeast.calculateEndPadding(direction)),
            top = ime.calculateTopPadding().coerceAtLeast(atLeast.calculateTopPadding()),
            bottom = ime.calculateBottomPadding().coerceAtLeast(atLeast.calculateBottomPadding()),
        )
    }.let(::padding)
}