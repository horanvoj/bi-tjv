package cz.fit.cvut.horanvoj.musicman.presentation.common

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.ui.Modifier

actual fun Modifier.imePadding(): Modifier = this
actual fun Modifier.statusBarsPadding(): Modifier = this
actual fun Modifier.navigationBarsPadding(): Modifier = this
actual fun Modifier.imePadding(atLeast: PaddingValues): Modifier = this.padding(atLeast)