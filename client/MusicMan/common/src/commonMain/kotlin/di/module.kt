package cz.fit.cvut.horanvoj.musicman.di

import cz.fit.cvut.horanvoj.musicman.infrastructure.infrastructureModule
import cz.fit.cvut.horanvoj.musicman.presentation.presentationModule
import org.kodein.di.DI

val di = DI {
    import(presentationModule)
    import(infrastructureModule)
}