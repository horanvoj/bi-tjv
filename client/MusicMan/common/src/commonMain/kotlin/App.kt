package cz.fit.cvut.horanvoj.musicman

import androidx.compose.runtime.Composable
import cz.fit.cvut.horanvoj.musicman.di.di
import cz.fit.cvut.horanvoj.musicman.presentation.root.AppContent
import cz.fit.cvut.horanvoj.musicman.presentation.theme.AppTheme
import org.kodein.di.compose.withDI

@Composable
fun App() = withDI(di) {
    AppTheme { AppContent() }
}