package cz.fit.cvut.horanvoj.musicman.infrastructure

import cz.fit.cvut.horanvoj.musicman.domain.model.ErrorResponse

data class ErrorResponseException(
    val errorResponse: ErrorResponse,
) : Exception()