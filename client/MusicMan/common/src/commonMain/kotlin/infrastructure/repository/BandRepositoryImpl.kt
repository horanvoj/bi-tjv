package cz.fit.cvut.horanvoj.musicman.infrastructure.repository

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.common.map
import cz.fit.cvut.horanvoj.musicman.domain.model.Band
import cz.fit.cvut.horanvoj.musicman.domain.repository.BandRepository
import cz.fit.cvut.horanvoj.musicman.infrastructure.api.BandApi
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.toDomain
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.toDto

internal class BandRepositoryImpl(
    private val bandApi: BandApi,
) : BandRepository {
    override suspend fun getBands(): Result<List<Band>> =
        bandApi.getBands().map { list -> list.map { it.toDomain() } }

    override suspend fun getBand(id: Long): Result<Band> =
        bandApi.getBand(id).map { it.toDomain() }

    override suspend fun addBand(band: Band): Result<Band> =
        bandApi.addBand(band.toDto()).map { it.toDomain() }

    override suspend fun removeBand(id: Long): Result<Unit> =
        bandApi.removeBand(id)

    override suspend fun changeBand(band: Band): Result<Band> =
        bandApi.changeBand(band.toDto()).map { it.toDomain() }
}