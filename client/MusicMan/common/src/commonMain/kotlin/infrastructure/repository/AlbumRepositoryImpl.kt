package cz.fit.cvut.horanvoj.musicman.infrastructure.repository

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.common.map
import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import cz.fit.cvut.horanvoj.musicman.domain.model.AlbumMedium
import cz.fit.cvut.horanvoj.musicman.domain.repository.AlbumRepository
import cz.fit.cvut.horanvoj.musicman.infrastructure.api.AlbumApi
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.toDomain
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.toDto

internal class AlbumRepositoryImpl(
    private val albumApi: AlbumApi,
) : AlbumRepository {
    override suspend fun getAlbums(): Result<List<Album>> =
        albumApi.getAlbums().map { list -> list.map { it.toDomain() } }

    override suspend fun getAlbum(id: Long): Result<Album> =
        albumApi.getAlbum(id).map { it.toDomain() }

    override suspend fun addAlbum(album: Album): Result<Album> =
        albumApi.addAlbum(album.toDto()).map { it.toDomain() }

    override suspend fun removeAlbum(id: Long): Result<Unit> =
        albumApi.removeAlbum(id)

    override suspend fun changeAlbum(album: Album): Result<Album> =
        albumApi.changeAlbum(album.toDto()).map { it.toDomain() }

    override suspend fun getMediums(id: Long, digitalQuality: Int): Result<AlbumMedium> =
        albumApi.getMediums(id, digitalQuality).map { it.toDomain() }

    override suspend fun addTrack(albumId: Long, trackId: Long): Result<Album> =
        albumApi.addTrack(albumId, trackId).map { it.toDomain() }

    override suspend fun removeTrack(albumId: Long, trackId: Long): Result<Album> =
        albumApi.removeTrack(albumId, trackId).map { it.toDomain() }
}