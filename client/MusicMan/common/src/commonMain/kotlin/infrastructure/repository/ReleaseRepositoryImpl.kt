package cz.fit.cvut.horanvoj.musicman.infrastructure.repository

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.common.map
import cz.fit.cvut.horanvoj.musicman.domain.model.Release
import cz.fit.cvut.horanvoj.musicman.domain.repository.ReleaseRepository
import cz.fit.cvut.horanvoj.musicman.infrastructure.api.ReleaseApi
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.toDomain
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.toDto

internal class ReleaseRepositoryImpl(
    private val releaseApi: ReleaseApi,
) : ReleaseRepository {
    override suspend fun getReleases(): Result<List<Release>> =
        releaseApi.getReleases().map { list -> list.map { it.toDomain() } }

    override suspend fun getRelease(id: Long): Result<Release> =
        releaseApi.getRelease(id).map { it.toDomain() }

    override suspend fun addRelease(release: Release): Result<Release> =
        releaseApi.addRelease(release.toDto()).map { it.toDomain() }

    override suspend fun removeRelease(id: Long): Result<Unit> =
        releaseApi.removeRelease(id)

    override suspend fun changeRelease(release: Release): Result<Release> =
        releaseApi.changeRelease(release.toDto()).map { it.toDomain() }
}