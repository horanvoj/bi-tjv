package cz.fit.cvut.horanvoj.musicman.infrastructure.repository

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.common.map
import cz.fit.cvut.horanvoj.musicman.domain.model.Track
import cz.fit.cvut.horanvoj.musicman.domain.repository.TrackRepository
import cz.fit.cvut.horanvoj.musicman.infrastructure.api.TrackApi
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.toDomain
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.toDto

internal class TrackRepositoryImpl(
    private val trackApi: TrackApi,
) : TrackRepository {
    override suspend fun getTracks(): Result<List<Track>> =
        trackApi.getTracks().map { list -> list.map { it.toDomain() } }

    override suspend fun getTrack(id: Long): Result<Track> =
        trackApi.getTrack(id).map { it.toDomain() }

    override suspend fun addTrack(track: Track): Result<Track> =
        trackApi.addTrack(track.toDto()).map { it.toDomain() }

    override suspend fun removeTrack(id: Long): Result<Unit> =
        trackApi.removeTrack(id)

    override suspend fun changeTrack(track: Track): Result<Track> =
        trackApi.changeTrack(track.toDto()).map { it.toDomain() }
}