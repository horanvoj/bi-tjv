package cz.fit.cvut.horanvoj.musicman.infrastructure.api

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.AlbumDto
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.AlbumMediumDto
import cz.fit.cvut.horanvoj.musicman.infrastructure.withNetwork
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*

internal interface AlbumApi {
    suspend fun getAlbums(): Result<List<AlbumDto>>
    suspend fun getAlbum(id: Long): Result<AlbumDto>
    suspend fun addAlbum(albumDto: AlbumDto): Result<AlbumDto>
    suspend fun removeAlbum(id: Long): Result<Unit>
    suspend fun changeAlbum(albumDto: AlbumDto): Result<AlbumDto>
    suspend fun getMediums(id: Long, digitalQuality: Int): Result<AlbumMediumDto>
    suspend fun addTrack(albumId: Long, trackId: Long): Result<AlbumDto>
    suspend fun removeTrack(albumId: Long, trackId: Long): Result<AlbumDto>
}

internal class AlbumApiImpl(
    private val client: HttpClient,
) : AlbumApi {
    override suspend fun getAlbums(): Result<List<AlbumDto>> = withNetwork {
        client
            .get("/album")
            .body()
    }

    override suspend fun getAlbum(id: Long): Result<AlbumDto> = withNetwork {
        client
            .get("/album/$id")
            .body()
    }

    override suspend fun addAlbum(albumDto: AlbumDto): Result<AlbumDto> = withNetwork {
        client
            .post("/album") {
                setBody(albumDto)
            }
            .body()
    }

    override suspend fun removeAlbum(id: Long): Result<Unit> = withNetwork {
        client
            .delete("/album/$id")
            .body()
    }

    override suspend fun changeAlbum(albumDto: AlbumDto): Result<AlbumDto> = withNetwork {
        client
            .put("/album/${albumDto.id}") {
                setBody(albumDto)
            }
            .body()
    }

    override suspend fun getMediums(id: Long, digitalQuality: Int): Result<AlbumMediumDto> = withNetwork {
        client
            .get("/album/$id/medium") {
                parameter("digitalQuality", digitalQuality)
            }
            .body()
    }

    override suspend fun addTrack(albumId: Long, trackId: Long): Result<AlbumDto> = withNetwork {
        client
            .put("/album/$albumId/track/$trackId")
            .body()
    }

    override suspend fun removeTrack(albumId: Long, trackId: Long): Result<AlbumDto> = withNetwork {
        client
            .delete("/album/$albumId/track/$trackId")
            .body()
    }
}