package cz.fit.cvut.horanvoj.musicman.infrastructure.api

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.ReleaseDto
import cz.fit.cvut.horanvoj.musicman.infrastructure.withNetwork
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*

internal interface ReleaseApi {
    suspend fun getReleases(): Result<List<ReleaseDto>>
    suspend fun getRelease(id: Long): Result<ReleaseDto>
    suspend fun addRelease(releaseDto: ReleaseDto): Result<ReleaseDto>
    suspend fun removeRelease(id: Long): Result<Unit>
    suspend fun changeRelease(releaseDto: ReleaseDto): Result<ReleaseDto>
}

internal class ReleaseApiImpl(
    private val client: HttpClient,
) : ReleaseApi {
    override suspend fun getReleases(): Result<List<ReleaseDto>> = withNetwork {
        client
            .get("/release")
            .body()
    }

    override suspend fun getRelease(id: Long): Result<ReleaseDto> = withNetwork {
        client
            .get("/release/$id")
            .body()
    }

    override suspend fun addRelease(releaseDto: ReleaseDto): Result<ReleaseDto> = withNetwork {
        client
            .post("/release") {
                setBody(releaseDto)
            }
            .body()
    }

    override suspend fun removeRelease(id: Long): Result<Unit> = withNetwork {
        client
            .delete("/release/$id")
            .body()

    }

    override suspend fun changeRelease(releaseDto: ReleaseDto): Result<ReleaseDto> = withNetwork {
        client
            .put("/release/${releaseDto.id}") {
                setBody(releaseDto)
            }
            .body()
    }
}