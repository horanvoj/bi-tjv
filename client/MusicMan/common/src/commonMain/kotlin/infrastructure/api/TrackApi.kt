package cz.fit.cvut.horanvoj.musicman.infrastructure.api

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.TrackDto
import cz.fit.cvut.horanvoj.musicman.infrastructure.withNetwork
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*

internal interface TrackApi {
    suspend fun getTracks(): Result<List<TrackDto>>
    suspend fun getTrack(id: Long): Result<TrackDto>
    suspend fun addTrack(trackDto: TrackDto): Result<TrackDto>
    suspend fun removeTrack(id: Long): Result<Unit>
    suspend fun changeTrack(trackDto: TrackDto): Result<TrackDto>
}

internal class TrackApiImpl(
    private val client: HttpClient,
) : TrackApi {
    override suspend fun getTracks(): Result<List<TrackDto>> = withNetwork {
        client
            .get("/track")
            .body()
    }

    override suspend fun getTrack(id: Long): Result<TrackDto> = withNetwork {
        client
            .get("/track/$id")
            .body()
    }

    override suspend fun addTrack(trackDto: TrackDto): Result<TrackDto> = withNetwork {
        client
            .post("/track") {
                setBody(trackDto)
            }
            .body()
    }

    override suspend fun removeTrack(id: Long): Result<Unit> = withNetwork {
        client
            .delete("/track/$id")
            .body()

    }

    override suspend fun changeTrack(trackDto: TrackDto): Result<TrackDto> = withNetwork {
        client
            .put("/track/${trackDto.id}") {
                setBody(trackDto)
            }
            .body()
    }
}