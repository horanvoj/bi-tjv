package cz.fit.cvut.horanvoj.musicman.infrastructure.api

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.BandDto
import cz.fit.cvut.horanvoj.musicman.infrastructure.withNetwork
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*

internal interface BandApi {
    suspend fun getBands(): Result<List<BandDto>>
    suspend fun getBand(id: Long): Result<BandDto>
    suspend fun addBand(bandDto: BandDto): Result<BandDto>
    suspend fun removeBand(id: Long): Result<Unit>
    suspend fun changeBand(bandDto: BandDto): Result<BandDto>
}

internal class BandApiImpl(
    private val client: HttpClient,
) : BandApi {
    override suspend fun getBands(): Result<List<BandDto>> = withNetwork {
        client
            .get("/band")
            .body()
    }

    override suspend fun getBand(id: Long): Result<BandDto> = withNetwork {
        client
            .get("/band/$id")
            .body()
    }

    override suspend fun addBand(bandDto: BandDto): Result<BandDto> = withNetwork {
        client
            .post("/band") {
                setBody(bandDto)
            }
            .body()
    }

    override suspend fun removeBand(id: Long): Result<Unit> = withNetwork {
        client
            .delete("/band/$id")
            .body()

    }

    override suspend fun changeBand(bandDto: BandDto): Result<BandDto> = withNetwork {
        client
            .put("/band/${bandDto.id}") {
                setBody(bandDto)
            }
            .body()
    }
}