package cz.fit.cvut.horanvoj.musicman.infrastructure.dto

import cz.fit.cvut.horanvoj.musicman.domain.model.ErrorResponse
import kotlinx.serialization.Serializable

@Serializable
internal class ErrorResponseDto(
    val code: Int,
    val status: String,
    val message: String?,
)

internal fun ErrorResponseDto.toDomain() = ErrorResponse(
    code = code,
    status = status,
    message = message,
)