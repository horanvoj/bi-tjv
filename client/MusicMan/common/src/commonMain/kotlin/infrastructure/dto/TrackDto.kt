package cz.fit.cvut.horanvoj.musicman.infrastructure.dto

import cz.fit.cvut.horanvoj.musicman.domain.model.Track
import kotlinx.serialization.Serializable

@Serializable
internal data class TrackDto(
    val id: Long,
    val length: Int,
    val name: String,
)

internal fun TrackDto.toDomain() = Track(
    id = id,
    length = length,
    name = name,
)

internal fun Track.toDto() = TrackDto(
    id = id,
    length = length,
    name = name,
)