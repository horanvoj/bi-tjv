package cz.fit.cvut.horanvoj.musicman.infrastructure.dto

import cz.fit.cvut.horanvoj.musicman.domain.model.Release
import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Serializable

@Serializable
internal data class ReleaseDto(
    val id: Long,
    val date: LocalDateTime,
    val type: ReleaseTypeDto,
    val name: String?,
    val albumId: Long,
)

internal fun ReleaseDto.toDomain() = Release(
    id = id,
    date = date,
    type = type.toDomain(),
    name = name,
    albumId = albumId,
)

internal fun Release.toDto() = ReleaseDto(
    id = id,
    date = date,
    type = type.toDto(),
    name = name,
    albumId = albumId,
)