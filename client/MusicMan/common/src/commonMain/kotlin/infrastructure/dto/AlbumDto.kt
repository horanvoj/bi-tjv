package cz.fit.cvut.horanvoj.musicman.infrastructure.dto

import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Serializable

@Serializable
internal data class AlbumDto(
    val id: Long,
    val name: String,
    val releaseDate: LocalDateTime,
    val bandId: Long,
    val releases: List<ReleaseDto>,
    val tracks: List<TrackDto>,
)

internal fun AlbumDto.toDomain() = Album(
    id = id,
    name = name,
    releaseDate = releaseDate,
    bandId = bandId,
    releases = releases.map { it.toDomain() },
    tracks = tracks.map { it.toDomain() }
)

internal fun Album.toDto() = AlbumDto(
    id = id,
    name = name,
    releaseDate = releaseDate,
    bandId = bandId,
    releases = releases.map { it.toDto() },
    tracks = tracks.map { it.toDto() }
)