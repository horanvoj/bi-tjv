package cz.fit.cvut.horanvoj.musicman.infrastructure.dto

import cz.fit.cvut.horanvoj.musicman.domain.model.ReleaseType

internal enum class ReleaseTypeDto {
    LP, CD, Digital, Cassette,
}

internal fun ReleaseType.toDto() = when (this) {
    ReleaseType.LP -> cz.fit.cvut.horanvoj.musicman.infrastructure.dto.ReleaseTypeDto.LP
    ReleaseType.CD -> cz.fit.cvut.horanvoj.musicman.infrastructure.dto.ReleaseTypeDto.CD
    ReleaseType.Digital -> cz.fit.cvut.horanvoj.musicman.infrastructure.dto.ReleaseTypeDto.Digital
    ReleaseType.Cassette -> cz.fit.cvut.horanvoj.musicman.infrastructure.dto.ReleaseTypeDto.Cassette
}

internal fun ReleaseTypeDto.toDomain() = when (this) {
    ReleaseTypeDto.LP -> ReleaseType.LP
    ReleaseTypeDto.CD -> ReleaseType.CD
    ReleaseTypeDto.Digital -> ReleaseType.Digital
    ReleaseTypeDto.Cassette -> ReleaseType.Cassette
}