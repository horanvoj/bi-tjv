package cz.fit.cvut.horanvoj.musicman.infrastructure.dto

import cz.fit.cvut.horanvoj.musicman.domain.model.Band
import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Serializable

@Serializable
internal data class BandDto(
    val id: Long,
    val name: String,
    val creationDate: LocalDateTime,
    val endDate: LocalDateTime?,
    val bio: String?,
    val albums: List<AlbumDto>,
)

internal fun BandDto.toDomain() = Band(
    id = id,
    name = name,
    creationDate = creationDate,
    endDate = endDate,
    bio = bio,
    albums = albums.map { it.toDomain() }
)

internal fun Band.toDto() = BandDto(
    id = id,
    name = name,
    creationDate = creationDate,
    endDate = endDate,
    bio = bio,
    albums = albums.map { it.toDto() }
)