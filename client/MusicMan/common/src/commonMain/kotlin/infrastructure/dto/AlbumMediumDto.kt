package cz.fit.cvut.horanvoj.musicman.infrastructure.dto

import cz.fit.cvut.horanvoj.musicman.domain.model.AlbumMedium
import kotlinx.serialization.Serializable

@Serializable
internal data class AlbumMediumDto(
    val records: Int,
    val discs: Int,
    val size: Long,
    val tapes: Int,
)

internal fun AlbumMediumDto.toDomain() = AlbumMedium(
    records = records,
    discs = discs,
    size = size,
    tapes = tapes,
)

internal fun AlbumMedium.toDto() = AlbumMediumDto(
    records = records,
    discs = discs,
    size = size,
    tapes = tapes,
)