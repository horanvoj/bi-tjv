package cz.fit.cvut.horanvoj.musicman.infrastructure

import cz.fit.cvut.horanvoj.musicman.common.NetworkConnectionError
import cz.fit.cvut.horanvoj.musicman.common.NetworkFailError
import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.domain.model.toErrorResult
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.ErrorResponseDto
import cz.fit.cvut.horanvoj.musicman.infrastructure.dto.toDomain
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import java.net.ConnectException

inline fun <T : Any> withNetwork(
    block: () -> T
): Result<T> {
    return try {
        Result.Success(block())
    } catch (e: ErrorResponseException) {
        Result.Error(e.errorResponse.toErrorResult())
    } catch (e: ConnectException) {
        Result.Error(NetworkConnectionError)
    } catch (e: Exception) {
        Result.Error(NetworkFailError())
    }
}

fun createHttpClient(): HttpClient {
    return HttpClient {
        expectSuccess = true
        defaultRequest {
//            url("http://130.61.156.106:8080")
            url("http://localhost:8080")
            header("Content-Type", "application/json")
        }
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.BODY
        }
        install(ContentNegotiation) {
            json()
        }
        HttpResponseValidator {
            handleResponseExceptionWithRequest { cause, _ ->
                val response = when (cause) {
                    is ResponseException -> cause.response
                    else -> {
                        return@handleResponseExceptionWithRequest
                    }
                }
                throw ErrorResponseException(
                    response
                        .body<ErrorResponseDto>()
                        .toDomain()
                )
            }
        }
    }
}