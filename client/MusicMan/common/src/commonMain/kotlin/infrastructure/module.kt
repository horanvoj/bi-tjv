package cz.fit.cvut.horanvoj.musicman.infrastructure

import cz.fit.cvut.horanvoj.musicman.domain.repository.AlbumRepository
import cz.fit.cvut.horanvoj.musicman.domain.repository.BandRepository
import cz.fit.cvut.horanvoj.musicman.domain.repository.ReleaseRepository
import cz.fit.cvut.horanvoj.musicman.domain.repository.TrackRepository
import cz.fit.cvut.horanvoj.musicman.infrastructure.api.*
import cz.fit.cvut.horanvoj.musicman.infrastructure.repository.AlbumRepositoryImpl
import cz.fit.cvut.horanvoj.musicman.infrastructure.repository.BandRepositoryImpl
import cz.fit.cvut.horanvoj.musicman.infrastructure.repository.ReleaseRepositoryImpl
import cz.fit.cvut.horanvoj.musicman.infrastructure.repository.TrackRepositoryImpl
import org.kodein.di.DI
import org.kodein.di.bindSingleton
import org.kodein.di.instance

val infrastructureModule = DI.Module("infrastructure") {
    bindSingleton { createHttpClient() }

    // API
    bindSingleton<ReleaseApi> { ReleaseApiImpl(instance()) }
    bindSingleton<TrackApi> { TrackApiImpl(instance()) }
    bindSingleton<BandApi> { BandApiImpl(instance()) }
    bindSingleton<AlbumApi> { AlbumApiImpl(instance()) }

    // Repository
    bindSingleton<ReleaseRepository> { ReleaseRepositoryImpl(instance()) }
    bindSingleton<TrackRepository> { TrackRepositoryImpl(instance()) }
    bindSingleton<BandRepository> { BandRepositoryImpl(instance()) }
    bindSingleton<AlbumRepository> { AlbumRepositoryImpl(instance()) }
}