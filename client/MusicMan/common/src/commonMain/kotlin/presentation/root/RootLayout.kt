package cz.fit.cvut.horanvoj.musicman.presentation.root

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cafe.adriel.voyager.navigator.tab.LocalTabNavigator
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabNavigator

@Composable
internal fun RootLayout(
    navigator: Navigator = LocalNavigator.currentOrThrow,
    modifier: Modifier = Modifier,
    floatingActionButton: @Composable () -> Unit = {},
    content: @Composable (PaddingValues) -> Unit,
) {
    val onBackPressed: (() -> Unit)? =
        if (navigator.canPop) {
            { navigator.pop() }
        } else {
            null
        }

    val scaffoldState = rememberScaffoldState(
        snackbarHostState = LocalAppState.current.snackbarHostState
    )

    Scaffold(
        modifier = modifier,
        scaffoldState = scaffoldState,
        floatingActionButton = floatingActionButton,
        topBar = {
            val navigationIcon: @Composable (() -> Unit)? = if (onBackPressed != null) {
                {
                    BackNavigationIcon(onClick = onBackPressed)
                }
            } else {
                null
            }

            TopAppBar(
                title = { Text(navigator.lastItem.title) },
                navigationIcon = navigationIcon,
            )
        },
        snackbarHost = {
            SnackbarHost(it)
        },
        content = content,
    )
}

@Composable
private fun BackNavigationIcon(
    onClick: () -> Unit,
) {
    IconButton(
        onClick = onClick,
    ) {
        Icon(
            imageVector = Icons.Default.ArrowBack,
            contentDescription = null
        )
    }
}

@Composable
internal fun RowScope.TabNavigationItem(
    tab: Tab,
    tabNavigator: TabNavigator = LocalTabNavigator.current,
) {
    BottomNavigationItem(
        selected = tabNavigator.current == tab,
        onClick = { tabNavigator.current = tab },
        label = { Text(tab.options.title) },
        alwaysShowLabel = false,
        icon = { tab.options.icon?.let { Icon(painter = it, contentDescription = tab.options.title) } }
    )
}