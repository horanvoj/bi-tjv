package cz.fit.cvut.horanvoj.musicman.presentation.root

import cafe.adriel.voyager.core.screen.Screen

interface ScreenWithTitle : Screen {
    val screenTitle: String
}

/**
 * Extensions value on [Screen] for easy access to title if the receiver [Screen] is a [ScreenWithTitle].
 */
val Screen.title get() = (this as? ScreenWithTitle)?.screenTitle.orEmpty()
