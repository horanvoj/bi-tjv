package cz.fit.cvut.horanvoj.musicman.presentation.root

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cz.fit.cvut.horanvoj.musicman.presentation.theme.AppTheme
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceSmall

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun WindowHeader(
    onCloseClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    AppTheme {
        Surface(
            color = MaterialTheme.colors.primaryVariant,
            modifier = modifier
                .fillMaxWidth()
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .padding(SpaceSmall),
            ) {
                Text(
                    text = "MusicMan",
                    style = MaterialTheme.typography.subtitle1,
                    modifier = Modifier.padding(horizontal = 12.dp)
                )
                Spacer(modifier = Modifier.weight(1f))
                CompositionLocalProvider(
                    LocalMinimumTouchTargetEnforcement provides false
                ) {
                    IconButton(
                        onClick = onCloseClick,
                        modifier = Modifier
                            .size(36.dp)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Close,
                            contentDescription = null,
                        )
                    }
                }
            }
        }
    }
}