package cz.fit.cvut.horanvoj.musicman.presentation.root

import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember

@Composable
fun rememberAppState(): AppState {
    return remember {
        AppState(
            snackbarHostState = SnackbarHostState(),
        )
    }
}

val LocalAppState = compositionLocalOf<AppState> { throw IllegalStateException("State not setup") }

class AppState(
    val snackbarHostState: SnackbarHostState,
)