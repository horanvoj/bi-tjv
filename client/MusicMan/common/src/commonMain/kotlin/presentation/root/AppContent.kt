package cz.fit.cvut.horanvoj.musicman.presentation.root

import androidx.compose.foundation.layout.Box
import androidx.compose.material.BottomNavigation
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.navigator.tab.CurrentTab
import cafe.adriel.voyager.navigator.tab.TabNavigator
import cz.fit.cvut.horanvoj.musicman.presentation.album.AlbumTab
import cz.fit.cvut.horanvoj.musicman.presentation.band.BandTab
import cz.fit.cvut.horanvoj.musicman.presentation.common.imePadding
import cz.fit.cvut.horanvoj.musicman.presentation.common.navigationBarsPadding
import cz.fit.cvut.horanvoj.musicman.presentation.common.statusBarsPadding
import cz.fit.cvut.horanvoj.musicman.presentation.release.ReleaseTab
import cz.fit.cvut.horanvoj.musicman.presentation.track.TrackTab

@Composable
internal fun AppContent() {
    val appState = rememberAppState()
    CompositionLocalProvider(
        LocalAppState provides appState
    ) {
        TabNavigator(AlbumTab) {
            Surface(
                color = MaterialTheme.colors.primaryVariant,
            ) {
                Scaffold(
                    modifier = Modifier
                        .statusBarsPadding()
                        .navigationBarsPadding(),
                    content = { padding ->
                        Box(
                            modifier = Modifier
                                .imePadding(padding),
                        ) {
                            CurrentTab()
                        }
                    },
                    bottomBar = {
                        BottomNavigation {
                            TabNavigationItem(AlbumTab)
                            TabNavigationItem(BandTab)
                            TabNavigationItem(TrackTab)
                            TabNavigationItem(ReleaseTab)
                        }
                    }
                )
            }
        }
    }
}