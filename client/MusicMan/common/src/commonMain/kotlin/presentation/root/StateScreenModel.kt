package cz.fit.cvut.horanvoj.musicman.presentation.root

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

abstract class BaseScreenModel<State>(
    initialState: State,
) : ScreenModel {
    private val _stateFlow = MutableStateFlow(initialState)
    val state = _stateFlow.asStateFlow()

    protected val lastState get() = state.value

    protected fun update(transform: State.() -> State) {
        _stateFlow.value = transform(_stateFlow.value)
    }

    protected fun launchOnMain(block: suspend CoroutineScope.() -> Unit) {
        coroutineScope.launch(context = Dispatchers.Main, block = block)
    }

    protected fun launchOnIO(block: suspend CoroutineScope.() -> Unit) {
        coroutineScope.launch(context = Dispatchers.IO, block = block)
    }
}

@Composable
fun <State> BaseScreenModel<State>.collectState() = state.collectAsState()