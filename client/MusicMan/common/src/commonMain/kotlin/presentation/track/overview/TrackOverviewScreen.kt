package cz.fit.cvut.horanvoj.musicman.presentation.track.overview

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.kodein.rememberScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cz.fit.cvut.horanvoj.musicman.common.message
import cz.fit.cvut.horanvoj.musicman.presentation.common.collectFilterState
import cz.fit.cvut.horanvoj.musicman.presentation.common.component.SearchField
import cz.fit.cvut.horanvoj.musicman.presentation.root.LocalAppState
import cz.fit.cvut.horanvoj.musicman.presentation.root.ScreenWithTitle
import cz.fit.cvut.horanvoj.musicman.presentation.root.collectState
import cz.fit.cvut.horanvoj.musicman.presentation.track.add.AddTrackScreen
import cz.fit.cvut.horanvoj.musicman.presentation.track.component.TrackItem
import kotlinx.coroutines.flow.*

class TrackOverviewScreen : ScreenWithTitle {
    override val screenTitle: String = "Tracks"

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    override fun Content() {
        val model = rememberScreenModel<TrackOverviewScreenModel>()
        val state by model.collectState()
        val filterState by model.collectFilterState()
        val appState = LocalAppState.current
        val navigator = LocalNavigator.currentOrThrow

        LifecycleEffect(
            onStarted = {
                model.updateTracks()
            }
        )

        LaunchedEffect(state.error) {
            snapshotFlow { state.error }
                .filterNotNull()
                .map { it.message }
                .collect { errorMsg ->
                    appState.snackbarHostState.showSnackbar(errorMsg)
                    model.onErrorShown()
                }
        }

        LazyColumn(
            modifier = Modifier
                .fillMaxHeight()
        ) {
            stickyHeader {
                SearchField(
                    value = filterState.query,
                    onValueChange = model::onFilterQuery,
                    modifier = Modifier.fillMaxWidth()
                )
            }

            items(filterState.filtered) { track ->
                TrackItem(
                    track = track,
                    onEditClick = {
                        navigator push AddTrackScreen(track.id)
                    },
                    onDeleteClick = {
                        model.deleteTrack(track.id)
                    }
                )
                Divider()
            }
        }
    }
}