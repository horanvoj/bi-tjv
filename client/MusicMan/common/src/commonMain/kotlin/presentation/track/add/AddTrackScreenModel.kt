package cz.fit.cvut.horanvoj.musicman.presentation.track.add

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.Track
import cz.fit.cvut.horanvoj.musicman.domain.repository.TrackRepository
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseScreenModel

class AddTrackScreenModel(
    private val trackRepository: TrackRepository,
) : BaseScreenModel<AddTrackScreenState>(
    AddTrackScreenState()
) {
    fun onViewInitialized(id: Long?) {
        launchOnIO {
            if (id != null) {
                trackRepository.getTrack(id)
                    .getOrAbort {
                        update { copy(error = it) }
                        return@launchOnIO
                    }
                    .also(::updateState)
            }
        }
    }

    private fun updateState(track: Track) {
        update {
            copy(
                id = track.id,
                name = track.name,
                length = track.length.toString(),
            )
        }
    }

    fun onNameChange(name: String) {
        update { copy(name = name) }
    }

    fun onLengthChange(length: String) {
        length.toIntOrNull()?.let { update { copy(length = length) } }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }

    fun onSaveClick() {
        launchOnIO {
            lastState
                .toTrack()
                .let { track ->
                    when (lastState.isEditing) {
                        true -> trackRepository.changeTrack(track)
                        false -> trackRepository.addTrack(track)
                    }
                }
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also(::updateState)
                .also {
                    update { copy(saved = true) }
                }
        }
    }

    private fun AddTrackScreenState.toTrack() = Track(
        id = id,
        name = name,
        length = length.toInt(),
    )
}

data class AddTrackScreenState(
    internal val id: Long = -1L,
    val name: String = "",
    val length: String = "",
    val error: ErrorResult? = null,
    val saved: Boolean = false,
) {
    val isSaveEnabled = name.isNotBlank() && length.toIntOrNull()?.let { it > 0 } ?: false
    val isEditing: Boolean = id != -1L
}