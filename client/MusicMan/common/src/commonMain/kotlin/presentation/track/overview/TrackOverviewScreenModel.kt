package cz.fit.cvut.horanvoj.musicman.presentation.track.overview

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.Track
import cz.fit.cvut.horanvoj.musicman.domain.repository.TrackRepository
import cz.fit.cvut.horanvoj.musicman.presentation.common.FilteringScreenModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList

class TrackOverviewScreenModel(
    private val trackRepository: TrackRepository
) : FilteringScreenModel<TrackOverviewScreenState, Track>(TrackOverviewScreenState()) {
    fun updateTracks() {
        launchOnIO {
            trackRepository
                .getTracks()
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also { tracks ->
                    update { copy(tracks = tracks.toPersistentList()) }
                    onCollectionUpdate(tracks)
                }
        }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }

    fun deleteTrack(id: Long) {
        launchOnIO {
            trackRepository
                .removeTrack(id)
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .let {
                    lastState
                        .tracks
                        .filterNot { it.id == id }
                        .toPersistentList()
                }
                .also { withoutTrack ->
                    update { copy(tracks = withoutTrack) }
                    onCollectionUpdate(withoutTrack)
                }
        }
    }
}

data class TrackOverviewScreenState(
    val tracks: ImmutableList<Track> = persistentListOf(),
    val error: ErrorResult? = null,
)