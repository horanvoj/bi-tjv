package cz.fit.cvut.horanvoj.musicman.presentation.track

import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.LibraryMusic
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import cafe.adriel.voyager.navigator.CurrentScreen
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseNavigator
import cz.fit.cvut.horanvoj.musicman.presentation.root.RootLayout
import cz.fit.cvut.horanvoj.musicman.presentation.track.add.AddTrackScreen
import cz.fit.cvut.horanvoj.musicman.presentation.track.overview.TrackOverviewScreen

object TrackTab : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val icon = rememberVectorPainter(Icons.Default.LibraryMusic)
            return remember {
                TabOptions(
                    index = 0u,
                    icon = icon,
                    title = "Tracks"
                )
            }
        }

    @Composable
    override fun Content() {
        BaseNavigator(TrackOverviewScreen()) { navigator ->
            RootLayout(
                floatingActionButton = {
                    if (navigator.lastItem is TrackOverviewScreen) {
                        FloatingActionButton(
                            onClick = { navigator.push(AddTrackScreen()) },
                        ) {
                            Icon(
                                painter = rememberVectorPainter(Icons.Default.Add),
                                contentDescription = null,
                            )
                        }
                    }
                },
            ) {
                CurrentScreen()
            }
        }
    }
}