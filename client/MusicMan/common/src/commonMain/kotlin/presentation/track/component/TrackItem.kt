package cz.fit.cvut.horanvoj.musicman.presentation.track.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import cz.fit.cvut.horanvoj.musicman.domain.model.Track
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceMedium

@OptIn(ExperimentalMaterialApi::class)
@Composable
internal fun TrackItem(
    track: Track,
    modifier: Modifier = Modifier,
    onEditClick: (() -> Unit)? = null,
    onDeleteClick: (() -> Unit)? = null,
    onClick: (() -> Unit)? = null,
) {
    Column(
        modifier = modifier
            .then(
                if (onClick != null) {
                    Modifier.clickable { onClick() }
                } else {
                    Modifier
                }
            ),
    ) {
        ListItem(
            secondaryText = {
                Text(text = track.duration.toString())
            },
            text = {
                Text(text = track.name)
            },
        )
        Row(
            modifier = Modifier
                .align(Alignment.End)
                .padding(horizontal = SpaceMedium)
        ) {
            onDeleteClick?.let { onClick ->
                TextButton(
                    onClick = onClick,
                    modifier = Modifier
                        .padding(horizontal = SpaceMedium)
                ) {
                    Text("Delete")
                }
            }

            onEditClick?.let { onClick ->
                TextButton(
                    onClick = onClick,
                    modifier = Modifier
                        .padding(horizontal = SpaceMedium)
                ) {
                    Text("Edit")
                }
            }
        }
    }
}