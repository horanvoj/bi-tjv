package cz.fit.cvut.horanvoj.musicman.presentation.track.add

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.kodein.rememberScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cz.fit.cvut.horanvoj.musicman.common.message
import cz.fit.cvut.horanvoj.musicman.presentation.root.LocalAppState
import cz.fit.cvut.horanvoj.musicman.presentation.root.ScreenWithTitle
import cz.fit.cvut.horanvoj.musicman.presentation.root.collectState
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceLarge
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map

class AddTrackScreen(
    val id: Long? = null,
) : ScreenWithTitle {
    override val screenTitle: String = "Track"

    @Composable
    override fun Content() {
        val model = rememberScreenModel<AddTrackScreenModel>()
        val state by model.collectState()
        val appState = LocalAppState.current
        val navigator = LocalNavigator.currentOrThrow

        LifecycleEffect(
            onStarted = {
                model.onViewInitialized(id)
            }
        )

        LaunchedEffect(state.error) {
            snapshotFlow { state.error }
                .filterNotNull()
                .map { it.message }
                .collect { errorMsg ->
                    appState.snackbarHostState.showSnackbar(errorMsg)
                    model.onErrorShown()
                }
        }

        LaunchedEffect(state.saved) {
            snapshotFlow { state.saved }
                .filter { it }
                .collectLatest {
                    navigator.pop()
                }
        }

        Column(
            verticalArrangement = Arrangement
                .spacedBy(SpaceLarge),
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(SpaceLarge),
        ) {
            TextField(
                value = state.name,
                label = { Text("Name") },
                singleLine = true,
                onValueChange = model::onNameChange,
                modifier = Modifier.fillMaxWidth(),
            )

            TextField(
                value = state.length,
                label = { Text("Length (in seconds)") },
                singleLine = true,
                onValueChange = model::onLengthChange,
                modifier = Modifier.fillMaxWidth(),
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number
                ),
            )


            Button(
                onClick = model::onSaveClick,
                enabled = state.isSaveEnabled,
                modifier = Modifier
                    .align(Alignment.End),
            ) {
                Text("Save")
            }
        }
    }
}