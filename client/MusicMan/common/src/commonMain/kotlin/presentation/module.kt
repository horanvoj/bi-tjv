package cz.fit.cvut.horanvoj.musicman.presentation

import cz.fit.cvut.horanvoj.musicman.presentation.album.add.AddAlbumScreenModel
import cz.fit.cvut.horanvoj.musicman.presentation.album.detail.AlbumDetailScreenModel
import cz.fit.cvut.horanvoj.musicman.presentation.album.overview.AlbumOverviewScreenModel
import cz.fit.cvut.horanvoj.musicman.presentation.band.add.AddBandScreenModel
import cz.fit.cvut.horanvoj.musicman.presentation.band.detail.BandDetailScreenModel
import cz.fit.cvut.horanvoj.musicman.presentation.band.overview.BandOverviewScreenModel
import cz.fit.cvut.horanvoj.musicman.presentation.release.add.AddReleaseScreenModel
import cz.fit.cvut.horanvoj.musicman.presentation.release.overview.ReleaseOverviewScreenModel
import cz.fit.cvut.horanvoj.musicman.presentation.track.add.AddTrackScreenModel
import cz.fit.cvut.horanvoj.musicman.presentation.track.overview.TrackOverviewScreenModel
import org.kodein.di.DI
import org.kodein.di.bindProvider
import org.kodein.di.instance

val presentationModule = DI.Module(name = "presentation") {
    bindProvider { ReleaseOverviewScreenModel(instance()) }
    bindProvider { AddReleaseScreenModel(instance(), instance()) }
    bindProvider { AlbumOverviewScreenModel(instance()) }
    bindProvider { AlbumDetailScreenModel(instance(), instance(), instance(), instance()) }
    bindProvider { AddAlbumScreenModel(instance()) }
    bindProvider { TrackOverviewScreenModel(instance()) }
    bindProvider { AddTrackScreenModel(instance()) }
    bindProvider { BandOverviewScreenModel(instance()) }
    bindProvider { BandDetailScreenModel(instance()) }
    bindProvider { AddBandScreenModel(instance()) }
}