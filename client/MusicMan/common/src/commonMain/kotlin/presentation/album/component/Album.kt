package cz.fit.cvut.horanvoj.musicman.presentation.album.component

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.OpenInNew
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import cz.fit.cvut.horanvoj.musicman.domain.model.Release
import cz.fit.cvut.horanvoj.musicman.domain.model.Track
import cz.fit.cvut.horanvoj.musicman.presentation.common.formatted
import cz.fit.cvut.horanvoj.musicman.presentation.release.component.ReleaseItem
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceLarge
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceMedium
import cz.fit.cvut.horanvoj.musicman.presentation.track.component.TrackItem

@Composable
fun Album(
    album: Album,
    bandName: String,
    onBandClick: () -> Unit,
    onAddReleaseClick: () -> Unit,
    onEditReleaseClick: (Release) -> Unit,
    onDeleteReleaseClick: (Release) -> Unit,
    onAddTrackToAlbumClick: () -> Unit,
    onEditTrackClick: (Track) -> Unit,
    onDeleteTrackFromAlbumClick: (Track) -> Unit,
    modifier: Modifier = Modifier,
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(SpaceMedium),
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .padding(vertical = SpaceLarge)
    ) {
        Text(
            text = album.name,
            style = MaterialTheme.typography.h5,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(horizontal = SpaceLarge)
        )
        TextButton(
            onClick = onBandClick,
            modifier = Modifier
                .padding(horizontal = SpaceLarge)
        ) {
            Icon(
                imageVector = Icons.Default.OpenInNew,
                contentDescription = null,
                modifier = Modifier.size(20.dp)
            )
            Text(
                text = bandName,
                modifier = Modifier
                    .padding(start = SpaceMedium)
            )
        }
        Text(
            text = album.releaseDate.formatted(),
            style = MaterialTheme.typography.subtitle1,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(horizontal = SpaceLarge)
        )

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(horizontal = SpaceLarge)
                .padding(top = SpaceMedium)
                .align(Alignment.Start)
        ) {
            Text(
                text = "Tracks",
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .weight(1f)
            )
            IconButton(
                onClick = onAddTrackToAlbumClick
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = null,
                )
            }
        }

        Column {
            album.tracks.forEach { track ->
                TrackItem(
                    track = track,
                    onDeleteClick = {
                        onDeleteTrackFromAlbumClick(track)
                    },
                    onEditClick = {
                        onEditTrackClick(track)
                    }
                )
                Divider()
            }
        }

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(horizontal = SpaceLarge)
                .padding(top = SpaceMedium)
                .align(Alignment.Start)
        ) {
            Text(
                text = "Releases",
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .weight(1f)
            )
            IconButton(
                onClick = onAddReleaseClick
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = null,
                )
            }
        }

        Column {
            album.releases.forEach { release ->
                ReleaseItem(
                    release = release,
                    onEditClick = {
                        onEditReleaseClick(release)
                    },
                    onDeleteClick = {
                        onDeleteReleaseClick(release)
                    },
                )
                Divider()
            }
        }
    }
}