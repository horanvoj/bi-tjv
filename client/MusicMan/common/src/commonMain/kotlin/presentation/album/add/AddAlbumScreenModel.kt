package cz.fit.cvut.horanvoj.musicman.presentation.album.add

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import cz.fit.cvut.horanvoj.musicman.domain.model.Release
import cz.fit.cvut.horanvoj.musicman.domain.model.Track
import cz.fit.cvut.horanvoj.musicman.domain.repository.AlbumRepository
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseScreenModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList
import kotlinx.datetime.LocalDateTime

class AddAlbumScreenModel(
    private val albumRepository: AlbumRepository,
) : BaseScreenModel<AddAlbumScreenState>(
    AddAlbumScreenState()
) {
    fun onViewInitialized(id: Long?, bandId: Long?) {
        launchOnIO {
            val albumBandId = id?.let {
                albumRepository.getAlbum(id)
                    .getOrAbort {
                        update { copy(error = it) }
                        return@launchOnIO
                    }
                    .also(::updateState)
                    .bandId
            } ?: bandId
            update { copy(bandId = albumBandId) }
        }
    }

    private fun updateState(album: Album) {
        update {
            copy(
                id = album.id,
                name = album.name,
                releaseDate = album.releaseDate,
                releases = album.releases.toPersistentList(),
                tracks = album.tracks.toPersistentList(),
            )
        }
    }

    fun onNameChange(name: String) {
        update { copy(name = name) }
    }

    fun onReleaseDateChange(date: LocalDateTime?) {
        update { copy(releaseDate = date) }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }

    fun onSaveClick() {
        launchOnIO {
            lastState
                .toAlbum()
                .let { album ->
                    when (lastState.isEditing) {
                        true -> albumRepository.changeAlbum(album)
                        false -> albumRepository.addAlbum(album)
                    }
                }
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also(::updateState)
                .also {
                    update { copy(saved = true) }
                }
        }
    }

    private fun AddAlbumScreenState.toAlbum() = Album(
        id = id,
        name = name,
        releaseDate = checkNotNull(releaseDate),
        bandId = checkNotNull(bandId),
        releases = releases,
        tracks = tracks,
    )
}

data class AddAlbumScreenState(
    internal val id: Long = -1L,
    val name: String = "",
    val releaseDate: LocalDateTime? = null,
    val bandId: Long? = null,
    internal val releases: ImmutableList<Release> = persistentListOf(),
    internal val tracks: ImmutableList<Track> = persistentListOf(),
    val error: ErrorResult? = null,
    val saved: Boolean = false,
) {
    val isSaveEnabled = name.isNotBlank() && releaseDate != null && bandId != null
    val isEditing: Boolean = id != -1L
}