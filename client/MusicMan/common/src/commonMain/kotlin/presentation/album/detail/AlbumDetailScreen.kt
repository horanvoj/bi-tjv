package cz.fit.cvut.horanvoj.musicman.presentation.album.detail

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshotFlow
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.kodein.rememberScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cz.fit.cvut.horanvoj.musicman.common.message
import cz.fit.cvut.horanvoj.musicman.presentation.album.component.AddTrackDialog
import cz.fit.cvut.horanvoj.musicman.presentation.album.component.Album
import cz.fit.cvut.horanvoj.musicman.presentation.band.detail.BandDetailScreen
import cz.fit.cvut.horanvoj.musicman.presentation.release.add.AddReleaseScreen
import cz.fit.cvut.horanvoj.musicman.presentation.root.LocalAppState
import cz.fit.cvut.horanvoj.musicman.presentation.root.ScreenWithTitle
import cz.fit.cvut.horanvoj.musicman.presentation.root.collectState
import cz.fit.cvut.horanvoj.musicman.presentation.track.add.AddTrackScreen
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map

class AlbumDetailScreen(
    private val albumId: Long,
) : ScreenWithTitle {
    override val screenTitle = "Album"

    @Composable
    override fun Content() {
        val model = rememberScreenModel<AlbumDetailScreenModel>()
        val state by model.collectState()
        val appState = LocalAppState.current
        val navigator = LocalNavigator.currentOrThrow

        LifecycleEffect(
            onStarted = {
                model.onViewInitialized(albumId)
            }
        )

        LaunchedEffect(state.error) {
            snapshotFlow { state.error }
                .filterNotNull()
                .map { it.message }
                .collect { errorMsg ->
                    appState.snackbarHostState.showSnackbar(errorMsg)
                    model.onErrorShown()
                }
        }

        state.album?.let { album ->
            Album(
                album = album,
                bandName = state.bandName,
                onBandClick = {
                    navigator push BandDetailScreen(album.bandId)
                },
                onAddTrackToAlbumClick = {
                    model.openAddTrackDialog()
                },
                onAddReleaseClick = {
                    navigator push AddReleaseScreen(id = null, albumId = album.id)
                },
                onDeleteTrackFromAlbumClick = { track ->
                    model.deleteTrack(track)
                },
                onDeleteReleaseClick = {
                    model.deleteRelease(it.id)
                },
                onEditReleaseClick = {
                    navigator push AddReleaseScreen(id = it.id, albumId = album.id)
                },
                onEditTrackClick = {
                    navigator push AddTrackScreen(id = it.id)
                }
            )
        }

        if (state.isAddTrackDialogVisible) {
            AddTrackDialog(
                tracks = state.tracks,
                onTrackClick = {
                    model.addTrack(it)
                },
                onDismiss = {
                    model.onTrackDialogDismiss()
                }
            )
        }
    }
}