package cz.fit.cvut.horanvoj.musicman.presentation.album.overview

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import cz.fit.cvut.horanvoj.musicman.domain.repository.AlbumRepository
import cz.fit.cvut.horanvoj.musicman.presentation.common.FilteringScreenModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList

class AlbumOverviewScreenModel(
    private val albumRepository: AlbumRepository,
) : FilteringScreenModel<AlbumOverviewScreenState, Album>(AlbumOverviewScreenState()) {
    fun updateAlbums() {
        launchOnIO {
            albumRepository
                .getAlbums()
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also { albums ->
                    update { copy(albums = albums.toPersistentList()) }
                    onCollectionUpdate(albums)
                }
        }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }

    fun deleteAlbum(id: Long) {
        launchOnIO {
            albumRepository
                .removeAlbum(id)
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .let {
                    lastState
                        .albums
                        .filterNot { it.id == id }
                        .toPersistentList()
                }
                .also { withoutAlbum ->
                    update { copy(albums = withoutAlbum) }
                    onCollectionUpdate(withoutAlbum)
                }
        }
    }
}

data class AlbumOverviewScreenState(
    val albums: ImmutableList<Album> = persistentListOf(),
    val error: ErrorResult? = null,
)