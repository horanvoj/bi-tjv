package cz.fit.cvut.horanvoj.musicman.presentation.album.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cz.fit.cvut.horanvoj.musicman.domain.model.Track
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceMedium
import cz.fit.cvut.horanvoj.musicman.presentation.track.component.TrackItem
import kotlinx.collections.immutable.ImmutableList

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AddTrackDialog(
    onTrackClick: (Track) -> Unit,
    onDismiss: () -> Unit,
    tracks: ImmutableList<Track>,
) {
    AlertDialog(
        modifier = Modifier
            .fillMaxWidth(),
        onDismissRequest = onDismiss,
        confirmButton = {
            TextButton(
                onClick = onDismiss,
            ) {
                Text(text = "Close")
            }
        },
        title = {
            Text(text = "Add track")
        },
        text = {
            Column(
                modifier = Modifier
                    .padding(top = SpaceMedium)
                    .verticalScroll(rememberScrollState())
            ) {
                if (tracks.isEmpty()) {
                    Text("No tracks can be added")
                } else {
                    tracks.forEach { track ->
                        TrackItem(
                            track = track,
                            onClick = {
                                onTrackClick(track)
                            }
                        )
                        Divider()
                    }
                }
            }
        }
    )
}