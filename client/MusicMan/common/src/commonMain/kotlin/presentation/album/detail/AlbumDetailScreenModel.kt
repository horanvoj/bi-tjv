package cz.fit.cvut.horanvoj.musicman.presentation.album.detail

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import cz.fit.cvut.horanvoj.musicman.domain.model.Track
import cz.fit.cvut.horanvoj.musicman.domain.repository.AlbumRepository
import cz.fit.cvut.horanvoj.musicman.domain.repository.BandRepository
import cz.fit.cvut.horanvoj.musicman.domain.repository.ReleaseRepository
import cz.fit.cvut.horanvoj.musicman.domain.repository.TrackRepository
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseScreenModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList

class AlbumDetailScreenModel(
    private val albumRepository: AlbumRepository,
    private val bandRepository: BandRepository,
    private val trackRepository: TrackRepository,
    private val releaseRepository: ReleaseRepository,
) : BaseScreenModel<AlbumDetailScreenState>(AlbumDetailScreenState()) {
    fun onViewInitialized(albumId: Long) {
        updateAlbum(albumId)
    }

    private fun updateAlbum(id: Long) {
        launchOnIO {
            albumRepository
                .getAlbum(id)
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also { album ->
                    update { copy(album = album) }
                }.also { album ->
                    bandRepository
                        .getBand(album.bandId)
                        .getOrAbort {
                            update { copy(error = it) }
                            return@launchOnIO
                        }
                        .also { band ->
                            update { copy(bandName = band.name) }
                        }
                }
        }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }

    fun deleteTrack(track: Track) {
        launchOnIO {
            lastState
                .album
                ?.id
                ?.let { albumId ->
                    albumRepository
                        .removeTrack(albumId, track.id)
                }
                ?.getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                ?.also { album ->
                    update { copy(album = album) }
                }
        }
    }

    fun deleteRelease(id: Long) {
        launchOnIO {
            releaseRepository
                .removeRelease(id)
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .let {
                    lastState
                        .album
                        ?.releases
                        ?.filterNot { it.id == id }
                }
                ?.also { withoutRelease ->
                    update {
                        copy(
                            album = album?.copy(releases = withoutRelease)
                        )
                    }
                }
        }
    }

    fun addTrack(track: Track) {
        launchOnIO {
            lastState
                .album
                ?.id
                ?.let { albumId ->
                    albumRepository
                        .addTrack(albumId, track.id)
                }
                ?.getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                ?.also { album ->
                    update {
                        copy(
                            album = album,
                            isAddTrackDialogVisible = false
                        )
                    }
                }
        }
    }

    fun onTrackDialogDismiss() {
        update { copy(isAddTrackDialogVisible = false) }
    }

    fun openAddTrackDialog() {
        launchOnIO {
            trackRepository
                .getTracks()
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .filterNot { lastState.album?.tracks?.contains(it) ?: true }
                .also { tracks ->
                    update {
                        copy(
                            tracks = tracks.toPersistentList(),
                            isAddTrackDialogVisible = true,
                        )
                    }
                }
        }
    }
}

data class AlbumDetailScreenState(
    val isAddTrackDialogVisible: Boolean = false,
    val album: Album? = null,
    val bandName: String = "",
    val error: ErrorResult? = null,
    val tracks: ImmutableList<Track> = persistentListOf(),
)