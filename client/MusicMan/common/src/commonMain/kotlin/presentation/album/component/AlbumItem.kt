package cz.fit.cvut.horanvoj.musicman.presentation.album.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import cz.fit.cvut.horanvoj.musicman.presentation.common.formatted
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceMedium

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AlbumItem(
    album: Album,
    onClick: () -> Unit,
    onEditClick: (() -> Unit)? = null,
    onDeleteClick: (() -> Unit)? = null,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier
            .clickable { onClick() }
    ) {
        ListItem(
            text = {
                Text(text = album.name)
            },
            trailing = {
                Text(text = "${album.totalDuration} total")
            },
            secondaryText = {
                Text(text = album.releaseDate.formatted())
            },
            overlineText = {
                Text(text = "${album.tracks.size} tracks, ${album.releases.size} releases")
            }
        )

        Row(
            modifier = Modifier
                .align(Alignment.End)
                .padding(horizontal = SpaceMedium)
        ) {
            onDeleteClick?.let { onClick ->
                TextButton(
                    onClick = onClick,
                    modifier = Modifier
                        .padding(horizontal = SpaceMedium)
                ) {
                    Text("Delete")
                }
            }

            onEditClick?.let { onClick ->
                TextButton(
                    onClick = onClick,
                    modifier = Modifier
                        .padding(horizontal = SpaceMedium)
                ) {
                    Text("Edit")
                }
            }
        }
    }
}