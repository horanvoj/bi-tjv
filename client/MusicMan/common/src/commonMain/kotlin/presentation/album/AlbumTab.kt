package cz.fit.cvut.horanvoj.musicman.presentation.album

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Album
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import cafe.adriel.voyager.navigator.CurrentScreen
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import cz.fit.cvut.horanvoj.musicman.presentation.album.overview.AlbumOverviewScreen
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseNavigator
import cz.fit.cvut.horanvoj.musicman.presentation.root.RootLayout

object AlbumTab : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val icon = rememberVectorPainter(Icons.Default.Album)
            return remember {
                TabOptions(
                    index = 0u,
                    icon = icon,
                    title = "Albums"
                )
            }
        }

    @Composable
    override fun Content() {
        BaseNavigator(AlbumOverviewScreen()) { navigator ->
            RootLayout {
                CurrentScreen()
            }
        }
    }
}