package cz.fit.cvut.horanvoj.musicman.presentation.album.add

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.kodein.rememberScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cz.fit.cvut.horanvoj.musicman.common.message
import cz.fit.cvut.horanvoj.musicman.presentation.common.component.DateField
import cz.fit.cvut.horanvoj.musicman.presentation.common.component.rememberDateFieldState
import cz.fit.cvut.horanvoj.musicman.presentation.root.LocalAppState
import cz.fit.cvut.horanvoj.musicman.presentation.root.ScreenWithTitle
import cz.fit.cvut.horanvoj.musicman.presentation.root.collectState
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceLarge
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map

class AddAlbumScreen(
    val id: Long? = null,
    val bandId: Long? = null,
) : ScreenWithTitle {
    override val screenTitle: String = "Album"

    @Composable
    override fun Content() {
        val model = rememberScreenModel<AddAlbumScreenModel>()
        val state by model.collectState()
        val appState = LocalAppState.current
        val navigator = LocalNavigator.currentOrThrow
        val releaseDateFieldState = rememberDateFieldState {
            model.onReleaseDateChange(it)
        }

        LifecycleEffect(
            onStarted = {
                model.onViewInitialized(id, bandId)
            }
        )

        LaunchedEffect(state.error) {
            snapshotFlow { state.error }
                .filterNotNull()
                .map { it.message }
                .collect { errorMsg ->
                    appState.snackbarHostState.showSnackbar(errorMsg)
                    model.onErrorShown()
                }
        }

        LaunchedEffect(state.saved) {
            snapshotFlow { state.saved }
                .filter { it }
                .collectLatest {
                    navigator.pop()
                }
        }

        // Setup with initial date
        LaunchedEffect(state.releaseDate) {
            snapshotFlow { state.releaseDate }
                .filterNotNull()
                .collectLatest {
                    if (releaseDateFieldState.dateTime == null) {
                        releaseDateFieldState.setDate(it)
                    }
                }
        }

        Column(
            verticalArrangement = Arrangement
                .spacedBy(SpaceLarge),
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(SpaceLarge),
        ) {
            TextField(
                value = state.name,
                label = { Text("Name") },
                singleLine = true,
                onValueChange = model::onNameChange,
                modifier = Modifier.fillMaxWidth(),
            )

            Text(
                text = "Release date",
                style = MaterialTheme.typography.caption
            )

            DateField(
                state = releaseDateFieldState,
            )

            Button(
                onClick = model::onSaveClick,
                enabled = state.isSaveEnabled,
                modifier = Modifier
                    .align(Alignment.End),
            ) {
                Text("Save")
            }
        }
    }
}