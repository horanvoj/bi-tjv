package cz.fit.cvut.horanvoj.musicman.presentation.common

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import cafe.adriel.voyager.core.model.coroutineScope
import cz.fit.cvut.horanvoj.musicman.domain.model.Filterable
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseScreenModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

abstract class FilteringScreenModel<State, T : Filterable>(initialState: State) :
    BaseScreenModel<State>(initialState) {
    private var job: Job? = null

    private val _filterState = MutableStateFlow(FilterState<T>())
    val filterState = _filterState.asStateFlow()

    protected fun onCollectionUpdate(collection: List<T>) {
        _filterState.value = _filterState.value.copy(
            collection = collection.toPersistentList(),
        )
        onFilterQuery(_filterState.value.query)
    }

    fun onFilterQuery(
        query: String,
    ) {
        _filterState.value = _filterState.value.copy(
            query = query,
        )

        if (query.isEmpty()) {
            _filterState.value = _filterState.value.copy(
                filtered = _filterState.value.collection
            )
            return
        }

        job?.cancel()
        job = coroutineScope.launch {
//            Enable if we would be running on a real server
//            delay(300)
            _filterState.value = _filterState.value.copy(
                filtered = _filterState.value
                    .collection
                    .filter { it.filter(query) }
                    .toPersistentList()
            )
        }
    }

    data class FilterState<T : Filterable>(
        val query: String = "",
        val filtered: ImmutableList<T> = persistentListOf(),
        internal val collection: ImmutableList<T> = persistentListOf(),
    )
}

@Composable
fun <State, T : Filterable> FilteringScreenModel<State, T>.collectFilterState() = filterState.collectAsState()