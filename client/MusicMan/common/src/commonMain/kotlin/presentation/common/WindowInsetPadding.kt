package cz.fit.cvut.horanvoj.musicman.presentation.common

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.ui.Modifier

expect fun Modifier.imePadding(): Modifier
expect fun Modifier.imePadding(atLeast: PaddingValues): Modifier
expect fun Modifier.statusBarsPadding(): Modifier
expect fun Modifier.navigationBarsPadding(): Modifier