package cz.fit.cvut.horanvoj.musicman.presentation.common.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceLarge
import kotlinx.datetime.LocalDateTime

@Composable
fun rememberDateFieldState(
    isRequired: Boolean = true,
    onDateChange: (LocalDateTime?) -> Unit,
): DateFieldState {
    val onDateChangeCurrent by rememberUpdatedState(onDateChange)
    return remember { DateFieldState(onDateChangeCurrent, isRequired) }
}

class DateFieldState internal constructor(
    private val onDateChange: (LocalDateTime?) -> Unit,
    isRequired: Boolean,
) {
    internal var day by mutableStateOf("")
    internal var month by mutableStateOf("")
    internal var year by mutableStateOf("")

    internal fun onDayChange(value: String) {
        day = value
        onDateChange(dateTime)
    }

    internal fun onMonthChange(value: String) {
        month = value
        onDateChange(dateTime)
    }

    internal fun onYearChange(value: String) {
        year = value
        onDateChange(dateTime)
    }

    fun setDate(date: LocalDateTime) {
        day = date.dayOfMonth.toString()
        month = date.monthNumber.toString()
        year = date.year.toString()
    }

    internal val isError by derivedStateOf { isRequired && dateTime == null }

    val dateTime by derivedStateOf {
        val dayInt = day.toIntOrNull()
        val monthInt = month.toIntOrNull()
        val yearInt = year.toIntOrNull()
        if (dayInt == null || monthInt == null || yearInt == null) {
            null
        } else {
            try {
                LocalDateTime(
                    year = yearInt,
                    monthNumber = monthInt,
                    dayOfMonth = dayInt,
                    hour = 0,
                    minute = 0
                )
            } catch (e: Exception) {
                null
            }
        }
    }
}

@Composable
fun DateField(
    state: DateFieldState,
    modifier: Modifier = Modifier,
) {
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(SpaceLarge)
    ) {
        TextField(
            value = state.day,
            label = { Text(text = "Day") },
            isError = state.isError,
            onValueChange = state::onDayChange,
            singleLine = true,
            modifier = Modifier.weight(1f),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number
            ),
        )
        TextField(
            value = state.month,
            label = { Text(text = "Month") },
            isError = state.isError,
            onValueChange = state::onMonthChange,
            singleLine = true,
            modifier = Modifier.weight(1f),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number
            ),
        )
        TextField(
            value = state.year,
            label = { Text(text = "Year") },
            isError = state.isError,
            onValueChange = state::onYearChange,
            singleLine = true,
            modifier = Modifier.weight(1f),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number
            ),
        )
    }
}