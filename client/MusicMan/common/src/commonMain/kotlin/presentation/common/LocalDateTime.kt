package cz.fit.cvut.horanvoj.musicman.presentation.common

import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.toJavaLocalDateTime
import java.time.format.DateTimeFormatter

fun LocalDateTime.formatted(): String {
    return toJavaLocalDateTime()
        .format(
            DateTimeFormatter.ofPattern("MMM. dd, yyyy")
        )
}