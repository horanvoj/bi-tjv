package cz.fit.cvut.horanvoj.musicman.presentation.band

import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Groups
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import cafe.adriel.voyager.navigator.CurrentScreen
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import cz.fit.cvut.horanvoj.musicman.presentation.band.add.AddBandScreen
import cz.fit.cvut.horanvoj.musicman.presentation.band.overview.BandOverviewScreen
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseNavigator
import cz.fit.cvut.horanvoj.musicman.presentation.root.RootLayout

object BandTab : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val icon = rememberVectorPainter(Icons.Default.Groups)
            return remember {
                TabOptions(
                    index = 0u,
                    icon = icon,
                    title = "Bands"
                )
            }
        }

    @Composable
    override fun Content() {
        BaseNavigator(BandOverviewScreen()) { navigator ->
            RootLayout(
                floatingActionButton = {
                    if (navigator.lastItem is BandOverviewScreen) {
                        FloatingActionButton(
                            onClick = { navigator.push(AddBandScreen()) },
                        ) {
                            Icon(
                                painter = rememberVectorPainter(Icons.Default.Add),
                                contentDescription = null,
                            )
                        }
                    }
                }
            ) {
                CurrentScreen()
            }
        }
    }
}