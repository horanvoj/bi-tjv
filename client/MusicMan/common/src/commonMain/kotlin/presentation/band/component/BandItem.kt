package cz.fit.cvut.horanvoj.musicman.presentation.band.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import cz.fit.cvut.horanvoj.musicman.domain.model.Band
import cz.fit.cvut.horanvoj.musicman.presentation.common.formatted
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceMedium

@OptIn(ExperimentalMaterialApi::class)
@Composable
internal fun BandItem(
    band: Band,
    onClick: () -> Unit,
    onEditClick: (() -> Unit)? = null,
    onDeleteClick: (() -> Unit)? = null,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .clickable { onClick() }
    ) {
        ListItem(
            singleLineSecondaryText = false,
            overlineText = {
                val text = band.creationDate.formatted().let { creation ->
                    band.endDate?.formatted()?.let { end ->
                        "$creation - $end"
                    } ?: creation
                }
                Text(text = text)
            },
            secondaryText = {
                Text(
                    text = band.bio ?: "No bio",
                    maxLines = 4,
                    overflow = TextOverflow.Ellipsis,
                )
            },
            trailing = {
                Text(text = "${band.albums.size} albums")
            },
            text = {
                Text(text = band.name)
            },
        )
        Row(
            modifier = Modifier
                .align(Alignment.End)
                .padding(horizontal = SpaceMedium)
        ) {
            onDeleteClick?.let { onClick ->
                TextButton(
                    onClick = onClick,
                    modifier = Modifier
                        .padding(horizontal = SpaceMedium)
                ) {
                    Text("Delete")
                }
            }

            onEditClick?.let { onClick ->
                TextButton(
                    onClick = onClick,
                    modifier = Modifier
                        .padding(horizontal = SpaceMedium)
                ) {
                    Text("Edit")
                }
            }
        }
    }
}