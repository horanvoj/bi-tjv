package cz.fit.cvut.horanvoj.musicman.presentation.band.add

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import cz.fit.cvut.horanvoj.musicman.domain.model.Band
import cz.fit.cvut.horanvoj.musicman.domain.repository.BandRepository
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseScreenModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList
import kotlinx.datetime.LocalDateTime

class AddBandScreenModel(
    private val bandRepository: BandRepository,
) : BaseScreenModel<AddBandScreenState>(
    AddBandScreenState()
) {
    fun onViewInitialized(id: Long?) {
        launchOnIO {
            if (id != null) {
                bandRepository.getBand(id)
                    .getOrAbort {
                        update { copy(error = it) }
                        return@launchOnIO
                    }
                    .also(::updateState)
            }
        }
    }

    private fun updateState(band: Band) {
        update {
            copy(
                id = band.id,
                name = band.name,
                creationDate = band.creationDate,
                endDate = band.endDate,
                bio = band.bio,
                albums = band.albums.toPersistentList()
            )
        }
    }

    fun onNameChange(name: String) {
        update { copy(name = name) }
    }

    fun onBioChange(bio: String) {
        update { copy(bio = bio) }
    }

    fun onCreationDateChange(date: LocalDateTime?) {
        update { copy(creationDate = date) }
    }

    fun onEndDateChange(date: LocalDateTime?) {
        update { copy(endDate = date) }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }

    fun onSaveClick() {
        launchOnIO {
            lastState
                .toBand()
                .let { band ->
                    when (lastState.isEditing) {
                        true -> bandRepository.changeBand(band)
                        false -> bandRepository.addBand(band)
                    }
                }
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also(::updateState)
                .also {
                    update { copy(saved = true) }
                }
        }
    }

    private fun AddBandScreenState.toBand() = Band(
        id = id,
        name = name,
        creationDate = checkNotNull(creationDate),
        endDate = endDate,
        bio = bio.takeIf { it.orEmpty().isNotBlank() },
        albums = albums
    )
}

data class AddBandScreenState(
    internal val id: Long = -1L,
    val name: String = "",
    val creationDate: LocalDateTime? = null,
    val endDate: LocalDateTime? = null,
    val bio: String? = null,
    internal val albums: ImmutableList<Album> = persistentListOf(),
    val error: ErrorResult? = null,
    val saved: Boolean = false,
) {
    val isSaveEnabled = name.isNotBlank() && creationDate != null
    val isEditing: Boolean = id != -1L
}