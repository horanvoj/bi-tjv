package cz.fit.cvut.horanvoj.musicman.presentation.band.detail

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.Band
import cz.fit.cvut.horanvoj.musicman.domain.repository.BandRepository
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseScreenModel

class BandDetailScreenModel(
    private val bandRepository: BandRepository,
) : BaseScreenModel<BandDetailScreenState>(BandDetailScreenState()) {
    fun onViewInitialized(bandId: Long) {
        updateBand(bandId)
    }

    private fun updateBand(id: Long) {
        launchOnIO {
            bandRepository
                .getBand(id)
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also { band ->
                    update { copy(band = band) }
                }
        }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }
}

data class BandDetailScreenState(
    val band: Band? = null,
    val error: ErrorResult? = null,
)