package cz.fit.cvut.horanvoj.musicman.presentation.band.detail

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshotFlow
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.kodein.rememberScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cz.fit.cvut.horanvoj.musicman.common.message
import cz.fit.cvut.horanvoj.musicman.presentation.album.add.AddAlbumScreen
import cz.fit.cvut.horanvoj.musicman.presentation.album.detail.AlbumDetailScreen
import cz.fit.cvut.horanvoj.musicman.presentation.band.component.Band
import cz.fit.cvut.horanvoj.musicman.presentation.root.LocalAppState
import cz.fit.cvut.horanvoj.musicman.presentation.root.ScreenWithTitle
import cz.fit.cvut.horanvoj.musicman.presentation.root.collectState
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map

class BandDetailScreen(
    private val bandId: Long,
) : ScreenWithTitle {
    override val screenTitle = "Band"

    @Composable
    override fun Content() {
        val model = rememberScreenModel<BandDetailScreenModel>()
        val state by model.collectState()
        val navigator = LocalNavigator.currentOrThrow
        val appState = LocalAppState.current

        LifecycleEffect(
            onStarted = {
                model.onViewInitialized(bandId)
            }
        )

        LaunchedEffect(state.error) {
            snapshotFlow { state.error }
                .filterNotNull()
                .map { it.message }
                .collect { errorMsg ->
                    appState.snackbarHostState.showSnackbar(errorMsg)
                    model.onErrorShown()
                }
        }

        state.band?.let { band ->
            Band(
                band = band,
                onAlbumClick = { album ->
                    navigator push AlbumDetailScreen(album.id)
                },
                onAddAlbumClick = {
                    navigator push AddAlbumScreen(bandId = band.id)
                },
                onAlbumEditClick = {
                    navigator push AddAlbumScreen(id = it.id, bandId = band.id)
                }
            )
        }
    }
}