package cz.fit.cvut.horanvoj.musicman.presentation.band.overview

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.Band
import cz.fit.cvut.horanvoj.musicman.domain.repository.BandRepository
import cz.fit.cvut.horanvoj.musicman.presentation.common.FilteringScreenModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList

class BandOverviewScreenModel(
    private val bandRepository: BandRepository
) : FilteringScreenModel<BandOverviewScreenState, Band>(BandOverviewScreenState()) {
    fun updateBands() {
        launchOnIO {
            bandRepository
                .getBands()
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also { bands ->
                    update { copy(bands = bands.toPersistentList()) }
                    onCollectionUpdate(bands)
                }
        }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }

    fun deleteBand(id: Long) {
        launchOnIO {
            bandRepository
                .removeBand(id)
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .let {
                    lastState
                        .bands
                        .filterNot { it.id == id }
                        .toPersistentList()
                }
                .also { withoutBand ->
                    update { copy(bands = withoutBand) }
                    onCollectionUpdate(withoutBand)
                }
        }
    }
}

data class BandOverviewScreenState(
    val bands: ImmutableList<Band> = persistentListOf(),
    val error: ErrorResult? = null,
)