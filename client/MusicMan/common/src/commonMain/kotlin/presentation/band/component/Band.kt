package cz.fit.cvut.horanvoj.musicman.presentation.band.component

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import cz.fit.cvut.horanvoj.musicman.domain.model.Band
import cz.fit.cvut.horanvoj.musicman.presentation.album.component.AlbumItem
import cz.fit.cvut.horanvoj.musicman.presentation.common.formatted
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceLarge
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceMedium

@Composable
fun Band(
    band: Band,
    onAlbumClick: (Album) -> Unit,
    onAlbumEditClick: (Album) -> Unit,
    onAddAlbumClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(SpaceMedium),
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .padding(vertical = SpaceLarge)
    ) {
        Text(
            text = band.name,
            style = MaterialTheme.typography.h5,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(horizontal = SpaceLarge)
        )
        val lifeText = band.creationDate.formatted().let { creation ->
            band.endDate?.formatted()?.let { end ->
                "$creation - $end"
            } ?: creation
        }

        Text(
            text = lifeText,
            style = MaterialTheme.typography.subtitle1,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(horizontal = SpaceLarge)
        )

        band.bio?.let { bio ->
            Text(
                text = "Bio",
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .padding(horizontal = SpaceLarge)
                    .padding(top = SpaceMedium)
                    .align(Alignment.Start)
            )

            Text(
                text = bio,
                style = MaterialTheme.typography.body2,
                textAlign = TextAlign.Justify,
                modifier = Modifier
                    .align(Alignment.Start)
                    .padding(horizontal = SpaceLarge),
            )
        }

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(horizontal = SpaceLarge)
                .padding(top = SpaceMedium)
                .align(Alignment.Start),
        ) {
            Text(
                text = "Albums",
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .weight(1f)
            )
            IconButton(
                onClick = onAddAlbumClick
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = null,
                )
            }
        }

        Column {
            band.albums.forEach { album ->
                AlbumItem(
                    album = album,
                    onClick = {
                        onAlbumClick(album)
                    },
                    onEditClick = {
                        onAlbumEditClick(album)
                    }
                )
                Divider()
            }
        }

    }
}