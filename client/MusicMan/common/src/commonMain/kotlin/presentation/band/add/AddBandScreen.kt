package cz.fit.cvut.horanvoj.musicman.presentation.band.add

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.kodein.rememberScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cz.fit.cvut.horanvoj.musicman.common.message
import cz.fit.cvut.horanvoj.musicman.presentation.common.component.DateField
import cz.fit.cvut.horanvoj.musicman.presentation.common.component.rememberDateFieldState
import cz.fit.cvut.horanvoj.musicman.presentation.root.LocalAppState
import cz.fit.cvut.horanvoj.musicman.presentation.root.ScreenWithTitle
import cz.fit.cvut.horanvoj.musicman.presentation.root.collectState
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceLarge
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map

class AddBandScreen(
    val id: Long? = null,
) : ScreenWithTitle {
    override val screenTitle: String = "Band"

    @Composable
    override fun Content() {
        val model = rememberScreenModel<AddBandScreenModel>()
        val state by model.collectState()
        val appState = LocalAppState.current
        val navigator = LocalNavigator.currentOrThrow
        val creationDateFieldState = rememberDateFieldState {
            model.onCreationDateChange(it)
        }
        val endDateFieldState = rememberDateFieldState(isRequired = false) {
            model.onEndDateChange(it)
        }

        LifecycleEffect(
            onStarted = {
                model.onViewInitialized(id)
            }
        )

        LaunchedEffect(state.error) {
            snapshotFlow { state.error }
                .filterNotNull()
                .map { it.message }
                .collect { errorMsg ->
                    appState.snackbarHostState.showSnackbar(errorMsg)
                    model.onErrorShown()
                }
        }

        LaunchedEffect(state.saved) {
            snapshotFlow { state.saved }
                .filter { it }
                .collectLatest {
                    navigator.pop()
                }
        }

        // Setup with initial date
        LaunchedEffect(state.creationDate) {
            snapshotFlow { state.creationDate }
                .filterNotNull()
                .collectLatest {
                    if (creationDateFieldState.dateTime == null) {
                        creationDateFieldState.setDate(it)
                    }
                }
        }

        // Setup with initial date
        LaunchedEffect(state.endDate) {
            snapshotFlow { state.endDate }
                .filterNotNull()
                .collectLatest {
                    if (endDateFieldState.dateTime == null) {
                        endDateFieldState.setDate(it)
                    }
                }
        }

        Column(
            verticalArrangement = Arrangement
                .spacedBy(SpaceLarge),
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(SpaceLarge),
        ) {
            TextField(
                value = state.name,
                label = { Text("Name") },
                singleLine = true,
                onValueChange = model::onNameChange,
                modifier = Modifier.fillMaxWidth(),
            )

            TextField(
                value = state.bio.orEmpty(),
                label = { Text("Bio") },
                onValueChange = model::onBioChange,
                modifier = Modifier.fillMaxWidth(),
            )

            Text(
                text = "Creation date",
                style = MaterialTheme.typography.caption
            )

            DateField(
                state = creationDateFieldState,
            )

            Text(
                text = "End date",
                style = MaterialTheme.typography.caption
            )
            DateField(
                state = endDateFieldState,
            )

            Button(
                onClick = model::onSaveClick,
                enabled = state.isSaveEnabled,
                modifier = Modifier
                    .align(Alignment.End),
            ) {
                Text("Save")
            }
        }
    }
}