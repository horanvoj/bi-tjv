package cz.fit.cvut.horanvoj.musicman.presentation.release

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.QueueMusic
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import cafe.adriel.voyager.navigator.CurrentScreen
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import cz.fit.cvut.horanvoj.musicman.presentation.release.overview.ReleaseOverviewScreen
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseNavigator
import cz.fit.cvut.horanvoj.musicman.presentation.root.RootLayout

object ReleaseTab : Tab {

    override val options: TabOptions
        @Composable
        get() {
            val icon = rememberVectorPainter(Icons.Default.QueueMusic)
            return remember {
                TabOptions(
                    index = 0u,
                    icon = icon,
                    title = "Releases"
                )
            }
        }

    @Composable
    override fun Content() {
        BaseNavigator(ReleaseOverviewScreen()) {
            RootLayout {
                CurrentScreen()
            }
        }
    }
}