package cz.fit.cvut.horanvoj.musicman.presentation.release.add

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.kodein.rememberScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cz.fit.cvut.horanvoj.musicman.common.message
import cz.fit.cvut.horanvoj.musicman.domain.model.ReleaseType
import cz.fit.cvut.horanvoj.musicman.presentation.common.component.DateField
import cz.fit.cvut.horanvoj.musicman.presentation.common.component.rememberDateFieldState
import cz.fit.cvut.horanvoj.musicman.presentation.root.LocalAppState
import cz.fit.cvut.horanvoj.musicman.presentation.root.ScreenWithTitle
import cz.fit.cvut.horanvoj.musicman.presentation.root.collectState
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceLarge
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map

class AddReleaseScreen(
    val id: Long? = null,
    val albumId: Long? = null,
) : ScreenWithTitle {
    override val screenTitle: String = "Release"

    @Composable
    override fun Content() {
        val model = rememberScreenModel<AddReleaseScreenModel>()
        val state by model.collectState()
        val appState = LocalAppState.current
        val navigator = LocalNavigator.currentOrThrow
        val dateFieldState = rememberDateFieldState {
            model.onDateChange(it)
        }

        LifecycleEffect(
            onStarted = {
                model.onViewInitialized(id, albumId)
            }
        )

        LaunchedEffect(state.error) {
            snapshotFlow { state.error }
                .filterNotNull()
                .map { it.message }
                .collect { errorMsg ->
                    appState.snackbarHostState.showSnackbar(errorMsg)
                    model.onErrorShown()
                }
        }

        LaunchedEffect(state.saved) {
            snapshotFlow { state.saved }
                .filter { it }
                .collectLatest {
                    navigator.pop()
                }
        }

        // Setup with initial date
        LaunchedEffect(state.date) {
            snapshotFlow { state.date }
                .filterNotNull()
                .collectLatest {
                    if (dateFieldState.dateTime == null) {
                        dateFieldState.setDate(it)
                    }
                }
        }

        Column(
            verticalArrangement = Arrangement
                .spacedBy(SpaceLarge),
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(SpaceLarge),
        ) {
            TextField(
                value = state.name,
                label = { Text("Name") },
                singleLine = true,
                onValueChange = model::onNameChange,
                modifier = Modifier.fillMaxWidth(),
            )

            Text(
                text = "Date of creation",
                style = MaterialTheme.typography.caption
            )

            DateField(
                state = dateFieldState,
                modifier = Modifier.fillMaxWidth()
            )

            Box {
                TextField(
                    value = state.type.name,
                    label = { Text("Type") },
                    readOnly = true,
                    enabled = false,
                    onValueChange = {},
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable { model.onReleaseDropdownShow() },
                    colors = TextFieldDefaults.textFieldColors(
                        disabledTextColor = LocalContentColor.current.copy(LocalContentAlpha.current)
                    ),
                )

                DropdownMenu(
                    expanded = state.isReleaseDropdownExpanded,
                    onDismissRequest = model::onReleaseDropdownDismiss,
                ) {
                    ReleaseType.values().forEach { releaseType ->
                        DropdownMenuItem(
                            onClick = {
                                model.onTypeChange(releaseType)
                            }
                        ) {
                            Text(releaseType.name)
                        }
                    }
                }
            }

            AnimatedVisibility(state.isQualitySelectorVisible) {
                TextField(
                    value = state.digitalQuality,
                    label = { Text("Quality (kbps)") },
                    singleLine = true,
                    onValueChange = model::onDigitalQualityChange,
                    modifier = Modifier.fillMaxWidth(),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number
                    ),
                )

            }

            if (state.albumMediumText != null) {
                Text(
                    text = "This release will contain of ${state.albumMediumText}.",
                    style = MaterialTheme.typography.subtitle1
                )
            }

            Button(
                onClick = model::onSaveClick,
                enabled = state.isSaveEnabled,
                modifier = Modifier
                    .align(Alignment.End),
            ) {
                Text("Save")
            }
        }
    }
}