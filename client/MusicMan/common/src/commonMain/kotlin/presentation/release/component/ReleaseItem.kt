package cz.fit.cvut.horanvoj.musicman.presentation.release.component

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import cz.fit.cvut.horanvoj.musicman.domain.model.Release
import cz.fit.cvut.horanvoj.musicman.presentation.common.formatted
import cz.fit.cvut.horanvoj.musicman.presentation.theme.SpaceMedium

@OptIn(ExperimentalMaterialApi::class)
@Composable
internal fun ReleaseItem(
    release: Release,
    modifier: Modifier = Modifier,
    onAlbumClick: (() -> Unit)? = null,
    onEditClick: (() -> Unit)? = null,
    onDeleteClick: (() -> Unit)? = null,
) {
    Column(
        modifier = modifier
    ) {
        ListItem(
            trailing = {
                Text(text = release.type.name)
            },
            secondaryText = {
                Text(text = release.date.formatted())
            },
            text = {
                Text(text = release.name ?: "No name")
            },
        )
        Row(
            modifier = Modifier
                .align(Alignment.End)
                .padding(horizontal = SpaceMedium)
        ) {
            onAlbumClick?.let { onClick ->
                TextButton(
                    onClick = onClick,
                    modifier = Modifier
                        .padding(horizontal = SpaceMedium)
                ) {
                    Text("Album")
                }
            }

            onDeleteClick?.let { onClick ->
                TextButton(
                    onClick = onClick,
                    modifier = Modifier
                        .padding(horizontal = SpaceMedium)
                ) {
                    Text("Delete")
                }
            }

            onEditClick?.let { onClick ->
                TextButton(
                    onClick = onClick,
                    modifier = Modifier
                        .padding(horizontal = SpaceMedium)
                ) {
                    Text("Edit")
                }
            }
        }
    }
}