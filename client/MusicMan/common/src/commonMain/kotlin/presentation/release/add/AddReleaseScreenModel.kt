package cz.fit.cvut.horanvoj.musicman.presentation.release.add

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.extension.bytesToHumanReadableSize
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.AlbumMedium
import cz.fit.cvut.horanvoj.musicman.domain.model.Release
import cz.fit.cvut.horanvoj.musicman.domain.model.ReleaseType
import cz.fit.cvut.horanvoj.musicman.domain.repository.AlbumRepository
import cz.fit.cvut.horanvoj.musicman.domain.repository.ReleaseRepository
import cz.fit.cvut.horanvoj.musicman.presentation.root.BaseScreenModel
import kotlinx.datetime.LocalDateTime

class AddReleaseScreenModel(
    private val releaseRepository: ReleaseRepository,
    private val albumRepository: AlbumRepository,
) : BaseScreenModel<AddReleaseScreenState>(
    AddReleaseScreenState()
) {
    fun onViewInitialized(id: Long?, albumId: Long?) {
        launchOnIO {
            val releaseAlbumId = id?.let {
                releaseRepository.getRelease(id)
                    .getOrAbort {
                        update { copy(error = it) }
                        return@launchOnIO
                    }
                    .also(::updateState)
                    .albumId
            } ?: albumId
            update { copy(albumId = releaseAlbumId) }
            updateMediums()
        }
    }

    private fun updateState(release: Release) {
        update {
            copy(
                id = release.id,
                name = release.name.orEmpty(),
                type = release.type,
                date = release.date,
            )
        }
    }

    fun onNameChange(name: String) {
        update { copy(name = name) }
    }

    fun onTypeChange(type: ReleaseType) {
        updateMediums()
        update {
            copy(
                type = type,
                isReleaseDropdownExpanded = false,
            )
        }
    }

    private fun updateMediums() {
        launchOnIO {
            lastState.albumId?.let { albumId ->
                lastState.digitalQuality.toIntOrNull()?.let { quality ->
                    albumRepository
                        .getMediums(albumId, quality)
                        .getOrAbort {
                            update { copy(error = it) }
                            return@launchOnIO
                        }
                        .also { update { copy(mediums = it) } }
                }
            }
        }
    }

    fun onDateChange(date: LocalDateTime?) {
        update {
            copy(
                date = date
            )
        }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }

    fun onSaveClick() {
        launchOnIO {
            lastState
                .toRelease()
                .let { release ->
                    when (lastState.isEditing) {
                        true -> releaseRepository.changeRelease(release)
                        false -> releaseRepository.addRelease(release)
                    }
                }
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also(::updateState)
                .also {
                    update { copy(saved = true) }
                }
        }
    }

    fun onReleaseDropdownDismiss() {
        update { copy(isReleaseDropdownExpanded = false) }
    }

    fun onReleaseDropdownShow() {
        update { copy(isReleaseDropdownExpanded = true) }
    }

    fun onDigitalQualityChange(quality: String) {
        update { copy(digitalQuality = quality) }
        updateMediums()
    }

    private fun AddReleaseScreenState.toRelease() = Release(
        id = id,
        name = name,
        albumId = checkNotNull(albumId),
        date = checkNotNull(date),
        type = type,
    )
}

data class AddReleaseScreenState(
    internal val id: Long = -1L,
    val name: String = "",
    val date: LocalDateTime? = null,
    val type: ReleaseType = ReleaseType.Digital,
    val isReleaseDropdownExpanded: Boolean = false,
    val isAlbumDropdownExpanded: Boolean = false,
    val mediums: AlbumMedium? = null,
    val error: ErrorResult? = null,
    val albumId: Long? = null,
    val saved: Boolean = false,
    val digitalQuality: String = "320",
) {
    val isSaveEnabled = date != null &&
            albumId != null

    val isEditing: Boolean = id != -1L

    val isQualitySelectorVisible = type == ReleaseType.Digital

    val albumMediumText = mediums?.let { medium ->
        when (type) {
            ReleaseType.LP -> "${medium.records} discs"
            ReleaseType.CD -> "${medium.discs} discs"
            ReleaseType.Digital -> medium.size.toDouble().bytesToHumanReadableSize()
            ReleaseType.Cassette -> "${medium.tapes} tapes"
        }
    }
}