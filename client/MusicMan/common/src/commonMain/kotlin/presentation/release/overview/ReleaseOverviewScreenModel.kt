package cz.fit.cvut.horanvoj.musicman.presentation.release.overview

import cz.fit.cvut.horanvoj.musicman.common.ErrorResult
import cz.fit.cvut.horanvoj.musicman.common.getOrAbort
import cz.fit.cvut.horanvoj.musicman.domain.model.Release
import cz.fit.cvut.horanvoj.musicman.domain.repository.ReleaseRepository
import cz.fit.cvut.horanvoj.musicman.presentation.common.FilteringScreenModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList

class ReleaseOverviewScreenModel(
    private val releaseRepository: ReleaseRepository
) : FilteringScreenModel<ReleaseScreenState, Release>(ReleaseScreenState()) {
    fun updateReleases() {
        launchOnIO {
            releaseRepository
                .getReleases()
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .also { releases ->
                    update { copy(releases = releases.toPersistentList()) }
                    onCollectionUpdate(releases)
                }
        }
    }

    fun onErrorShown() {
        update { copy(error = null) }
    }

    fun deleteRelease(id: Long) {
        launchOnIO {
            releaseRepository
                .removeRelease(id)
                .getOrAbort {
                    update { copy(error = it) }
                    return@launchOnIO
                }
                .let {
                    lastState
                        .releases
                        .filterNot { it.id == id }
                        .toPersistentList()
                }
                .also { withoutRelease ->
                    update { copy(releases = withoutRelease) }
                    onCollectionUpdate(withoutRelease)
                }
        }
    }
}

data class ReleaseScreenState(
    val releases: ImmutableList<Release> = persistentListOf(),
    val error: ErrorResult? = null,
)