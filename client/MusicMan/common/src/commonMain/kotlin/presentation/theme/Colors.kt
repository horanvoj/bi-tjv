package cz.fit.cvut.horanvoj.musicman.presentation.theme

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

val LightColors = lightColors(
    primary = Color(0xFFff3d00),
    primaryVariant = Color(0xFFc30000),
    secondary = Color(0xFFffc107),
    secondaryVariant = Color(0xFFc79100),
    onPrimary = Color.Black,
    onSecondary = Color.Black,
)

val DarkColors = darkColors(
    primary = Color(0xFFff3d00),
    primaryVariant = Color(0xFFc30000),
    secondary = Color(0xFFffc107),
    secondaryVariant = Color(0xFFc79100),
    onPrimary = Color.Black,
    onSecondary = Color.Black,
)