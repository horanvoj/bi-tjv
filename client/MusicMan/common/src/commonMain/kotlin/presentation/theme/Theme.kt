package cz.fit.cvut.horanvoj.musicman.presentation.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable

@Composable
internal fun AppTheme(
    isDark: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit,
) {
    MaterialTheme(
        colors = getAppColors(isDark),
        content = content,
    )
}

@Composable
fun getAppColors(
    isDark: Boolean,
): Colors {
    return if (isDark) {
        DarkColors
    } else {
        LightColors
    }
}