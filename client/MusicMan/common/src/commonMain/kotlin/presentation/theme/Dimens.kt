package cz.fit.cvut.horanvoj.musicman.presentation.theme

import androidx.compose.ui.unit.dp

val SpaceSmall = 2.dp
val SpaceMedium = 8.dp
val SpaceLarge = 16.dp