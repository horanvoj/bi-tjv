package cz.fit.cvut.horanvoj.musicman.common

sealed interface ErrorResult

object ObjectNullError : ErrorResult
object NetworkConnectionError : ErrorResult
data class NetworkFailError(
    val message: String? = null
) : ErrorResult

val ErrorResult.message: String
    get() = when (this) {
        is NetworkFailError -> message ?: "Network error occurred"
        ObjectNullError -> "Object was null"
        NetworkConnectionError -> "Could not connect to network"
    }