package cz.fit.cvut.horanvoj.musicman.common.extension

import kotlinx.datetime.Instant

fun Instant.Companion.parseOrNull(isoString: String): Instant? = try {
    parse(isoString)
} catch (_: Exception) {
    null
}