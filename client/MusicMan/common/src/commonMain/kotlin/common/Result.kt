package cz.fit.cvut.horanvoj.musicman.common

sealed interface Result<out T : Any> {
    data class Success<out T : Any>(val data: T) : Result<T>
    data class Error(val error: ErrorResult) : Result<Nothing>
}

inline fun <T : Any, E : Any> Result<T>.map(transform: (T) -> E): Result<E> =
    when (this) {
        is Result.Error -> this
        is Result.Success -> Result.Success(transform(data))
    }

inline fun <T : Any> Result<T>.getOrAbort(onAbort: (ErrorResult) -> Unit) =
    when (this) {
        is Result.Success -> data
        is Result.Error -> {
            onAbort(error)
            throw IllegalStateException()
        }
    }