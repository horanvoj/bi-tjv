package cz.fit.cvut.horanvoj.musicman.domain.repository

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.domain.model.Band

interface BandRepository {
    suspend fun getBands(): Result<List<Band>>
    suspend fun getBand(id: Long): Result<Band>
    suspend fun addBand(band: Band): Result<Band>
    suspend fun removeBand(id: Long): Result<Unit>
    suspend fun changeBand(band: Band): Result<Band>
}