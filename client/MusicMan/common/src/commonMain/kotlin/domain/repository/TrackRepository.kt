package cz.fit.cvut.horanvoj.musicman.domain.repository

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.domain.model.Track

interface TrackRepository {
    suspend fun getTracks(): Result<List<Track>>
    suspend fun getTrack(id: Long): Result<Track>
    suspend fun addTrack(track: Track): Result<Track>
    suspend fun removeTrack(id: Long): Result<Unit>
    suspend fun changeTrack(track: Track): Result<Track>
}