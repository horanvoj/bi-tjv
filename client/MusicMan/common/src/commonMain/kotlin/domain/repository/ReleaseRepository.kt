package cz.fit.cvut.horanvoj.musicman.domain.repository

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.domain.model.Release

interface ReleaseRepository {
    suspend fun getReleases(): Result<List<Release>>
    suspend fun getRelease(id: Long): Result<Release>
    suspend fun addRelease(release: Release): Result<Release>
    suspend fun removeRelease(id: Long): Result<Unit>
    suspend fun changeRelease(release: Release): Result<Release>
}