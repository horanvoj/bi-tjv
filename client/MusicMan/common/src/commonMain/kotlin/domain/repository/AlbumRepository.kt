package cz.fit.cvut.horanvoj.musicman.domain.repository

import cz.fit.cvut.horanvoj.musicman.common.Result
import cz.fit.cvut.horanvoj.musicman.domain.model.Album
import cz.fit.cvut.horanvoj.musicman.domain.model.AlbumMedium

interface AlbumRepository {
    suspend fun getAlbums(): Result<List<Album>>
    suspend fun getAlbum(id: Long): Result<Album>
    suspend fun addAlbum(album: Album): Result<Album>
    suspend fun removeAlbum(id: Long): Result<Unit>
    suspend fun changeAlbum(album: Album): Result<Album>
    suspend fun getMediums(id: Long, digitalQuality: Int): Result<AlbumMedium>
    suspend fun addTrack(albumId: Long, trackId: Long): Result<Album>
    suspend fun removeTrack(albumId: Long, trackId: Long): Result<Album>
}