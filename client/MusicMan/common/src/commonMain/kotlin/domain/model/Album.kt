package cz.fit.cvut.horanvoj.musicman.domain.model

import kotlinx.datetime.LocalDateTime
import kotlin.time.DurationUnit
import kotlin.time.toDuration

data class Album(
    val id: Long,
    val name: String,
    val releaseDate: LocalDateTime,
    val bandId: Long,
    val releases: List<Release>,
    val tracks: List<Track>,
) : Filterable {
    val totalDuration = tracks.sumOf { it.length }.toDuration(DurationUnit.SECONDS)

    override fun filter(query: String): Boolean {
        return name.contains(query, ignoreCase = true) ||
                releases.any { it.filter(query) } ||
                tracks.any { it.filter(query) }
    }
}
