package cz.fit.cvut.horanvoj.musicman.domain.model

import cz.fit.cvut.horanvoj.musicman.common.NetworkFailError

class ErrorResponse(
    val code: Int,
    val status: String,
    val message: String?,
)

fun ErrorResponse.toErrorResult() = NetworkFailError(message)