package cz.fit.cvut.horanvoj.musicman.domain.model

data class AlbumMedium(
    val records: Int,
    val discs: Int,
    val size: Long,
    val tapes: Int,
)