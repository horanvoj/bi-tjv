package cz.fit.cvut.horanvoj.musicman.domain.model

interface Filterable {
    /**
     * @return true if the element satisfies [query]
     */
    fun filter(query: String): Boolean
}