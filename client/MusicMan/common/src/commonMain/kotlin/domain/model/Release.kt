package cz.fit.cvut.horanvoj.musicman.domain.model

import kotlinx.datetime.LocalDateTime

data class Release(
    val id: Long,
    val date: LocalDateTime,
    val type: ReleaseType,
    val name: String?,
    val albumId: Long,
) : Filterable {
    override fun filter(query: String): Boolean {
        return name?.contains(query, ignoreCase = true) ?: false ||
                query.contains(type.name)
    }
}
