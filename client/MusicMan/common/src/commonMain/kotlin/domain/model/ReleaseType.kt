package cz.fit.cvut.horanvoj.musicman.domain.model

enum class ReleaseType {
    LP, CD, Digital, Cassette,
}