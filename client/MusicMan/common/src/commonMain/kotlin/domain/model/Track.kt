package cz.fit.cvut.horanvoj.musicman.domain.model

import kotlin.time.DurationUnit
import kotlin.time.toDuration

data class Track(
    val id: Long,
    val length: Int,
    val name: String,
) : Filterable {
    val duration = length.toDuration(DurationUnit.SECONDS)

    override fun filter(query: String): Boolean {
        return name.contains(query, ignoreCase = true)
    }
}
