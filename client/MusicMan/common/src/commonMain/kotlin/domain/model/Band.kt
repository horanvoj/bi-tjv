package cz.fit.cvut.horanvoj.musicman.domain.model

import kotlinx.datetime.LocalDateTime

data class Band(
    val id: Long,
    val name: String,
    val creationDate: LocalDateTime,
    val endDate: LocalDateTime?,
    val bio: String?,
    val albums: List<Album>,
) : Filterable {
    override fun filter(query: String): Boolean {
        return name.contains(query, ignoreCase = true) ||
                bio?.contains(query, ignoreCase = true) ?: false ||
                albums.any { it.filter(query) }
    }
}
