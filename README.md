# Music management

## Popis
Aplikace bude sloužit na jednoduchou správu hudební kolekce pro hudební vydavatelství. V kolekci si budeme moci spravovat kapely (pro zjednodušení nebudou existovat umělci jednotlivci), jejich alba a skladby které na nich jsou.

Vydavatelstvím to tedy usnadní práci, nebudou muset svoji kolekci spravovat v excelu nebo na papíře.

## Komplexnější business operace na serveru:
- Při vkládání entity `Band` a `Album` se ověří, že ještě neexistuje záznam s podobným jménem, pokud ano, entita nebude vytvořena.
- Pokud budeme chtít vydat album, musíme si zvolit formát. Aplikace nám bude umět vygenerovat informace o formátech pro dané album. Tj. Dlouhé album budeme chtít vydat na CD, aplikace nám vypíše, že k tomu budeme potřebovat tři CD. Obdobně s kazetou (počet pásek), LP (počet desek) i digitálním vydáním (velikost v MB při nějaké kvalitě).

## Komplexnější business operace v klientovi:
- Filtrování a vyhledávání
- Generování vydání alba (odpovídá dotazu nahoře)

## Diagram
![Diagram](/resources/diagram.png)
