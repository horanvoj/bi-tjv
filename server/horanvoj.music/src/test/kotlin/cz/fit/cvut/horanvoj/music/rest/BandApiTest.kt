package cz.fit.cvut.horanvoj.music.rest

import cz.fit.cvut.horanvoj.music.business.BandService
import cz.fit.cvut.horanvoj.music.common.EntityNameTooSimilarError
import cz.fit.cvut.horanvoj.music.common.Result
import cz.fit.cvut.horanvoj.music.common.extension.NoId
import cz.fit.cvut.horanvoj.music.domain.Band
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.argThat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDateTime

@SpringBootTest // We need to load DTO mapper dependencies
@AutoConfigureMockMvc
class BandApiTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var bandService: BandService

    @Test
    fun `ensure band with similar name cannot be created`() {
        val band = Band(Long.NoId, "Band", LocalDateTime.now(), null, null, listOf())
        Mockito
            .`when`(bandService.create(any()))
            .thenReturn(Result.Success(band))

        Mockito
            .`when`(bandService.create(argThat {
                this.name == band.name
            }))
            .thenReturn(Result.Error(EntityNameTooSimilarError(band.name)))

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post("/band")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(
                        "{\n" +
                                "  \"id\": 0,\n" +
                                "  \"name\": \"${band.name}\",\n" +
                                "  \"creationDate\": \"2023-01-03T19:59:04.927Z\",\n" +
                                "  \"endDate\": \"2023-01-03T19:59:04.927Z\",\n" +
                                "  \"bio\": \"string\",\n" +
                                "  \"albums\": []\n" +
                                "}"
                    )
            )
            .andExpect(MockMvcResultMatchers.status().isConflict)
    }
}