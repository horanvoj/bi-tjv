package cz.fit.cvut.horanvoj.music.dao

import cz.fit.cvut.horanvoj.music.common.extension.NoId
import cz.fit.cvut.horanvoj.music.domain.Track
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TrackRepositoryTest {
    @Autowired
    lateinit var trackRepository: TrackRepository

    @AfterEach
    fun tearDown() {
        trackRepository.deleteAll()
    }

    @Test
    fun `ensure adding track adds exactly one track`() {
        val track = Track(Long.NoId, 123, "Track one", listOf())
        val id = trackRepository.save(track).id

        val tracks = trackRepository.findAll()
        val jpaTrack = tracks.firstOrNull { it.id == id }
        Assertions.assertEquals(1, tracks.count())
        Assertions.assertNotNull(jpaTrack)
        jpaTrack ?: return
        Assertions.assertNotEquals(Long.NoId, jpaTrack.id)
        Assertions.assertEquals(track.copy(id = id), jpaTrack)
    }

    @Test
    fun `ensure removing track removes only the requested track`() {
        val trackOne = Track(Long.NoId, 123, "Track one", listOf())
        val trackTwo = Track(Long.NoId, 98, "Track two", listOf())

        val idOne = trackRepository.save(trackOne).id
        val idTwo = trackRepository.save(trackTwo).id

        Assertions.assertEquals(2, trackRepository.findAll().count())

        trackRepository.deleteById(idOne)

        val tracks = trackRepository.findAll()
        Assertions.assertEquals(1, tracks.count())
        Assertions.assertEquals(idTwo, tracks.firstOrNull()?.id)
    }

    @Test
    fun `ensure modifying track is correct`() {
        val track = Track(Long.NoId, 1, "Edit me", listOf())
        val jpaTrack = trackRepository.save(track)
        val jpaFoundTrack = trackRepository.findById(jpaTrack.id)

        Assertions.assertTrue(jpaFoundTrack.isPresent)
        Assertions.assertEquals(jpaTrack, jpaFoundTrack.get())

        val modified = jpaTrack.copy(
            length = 42,
            name = "Edited",
        )

        val saved = trackRepository.save(modified)
        Assertions.assertEquals(modified, saved)

        val jpaSaved = trackRepository.findById(modified.id)
        Assertions.assertTrue(jpaSaved.isPresent)
        Assertions.assertEquals(modified, jpaSaved.get())
    }
}