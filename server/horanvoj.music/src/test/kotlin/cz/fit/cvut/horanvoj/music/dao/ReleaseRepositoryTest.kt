package cz.fit.cvut.horanvoj.music.dao

import cz.fit.cvut.horanvoj.music.common.extension.NoId
import cz.fit.cvut.horanvoj.music.domain.Album
import cz.fit.cvut.horanvoj.music.domain.Band
import cz.fit.cvut.horanvoj.music.domain.Release
import cz.fit.cvut.horanvoj.music.domain.ReleaseType
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.time.LocalDateTime

@DataJpaTest
class ReleaseRepositoryTest {
    @Autowired
    lateinit var releaseRepository: ReleaseRepository

    @Autowired
    lateinit var bandRepository: BandRepository

    @Autowired
    lateinit var albumRepository: AlbumRepository

    @AfterEach
    fun tearDown() {
        releaseRepository.deleteAll()
        albumRepository.deleteAll()
        bandRepository.deleteAll()
    }

    @Test
    fun `ensure adding release adds exactly one release`() {
        val band = Band(Long.NoId, "Test band", LocalDateTime.now(), null, null, listOf())
        val jpaBand = bandRepository.save(band)

        val album = Album(Long.NoId, "Test album", LocalDateTime.now(), jpaBand, listOf(), listOf())
        val jpaAlbum = albumRepository.save(album)

        val release = Release(Long.NoId, LocalDateTime.now(), ReleaseType.CD, "Test release", jpaAlbum)

        val jpaRelease = releaseRepository
            .save(release)

        val foundJpaRelease = releaseRepository
            .findById(jpaRelease.id)

        Assertions
            .assertNotEquals(Long.NoId, jpaRelease.id)
        Assertions
            .assertTrue(foundJpaRelease.isPresent)
        Assertions
            .assertEquals(jpaRelease, foundJpaRelease.get())
        Assertions
            .assertEquals(jpaAlbum.id, foundJpaRelease.get().album.id)
        Assertions
            .assertEquals(jpaBand.id, foundJpaRelease.get().album.band.id)
    }
}