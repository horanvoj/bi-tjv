package cz.fit.cvut.horanvoj.music.business

import cz.fit.cvut.horanvoj.music.common.Result
import cz.fit.cvut.horanvoj.music.common.getOrThrow
import cz.fit.cvut.horanvoj.music.dao.AlbumRepository
import cz.fit.cvut.horanvoj.music.dao.TrackRepository
import cz.fit.cvut.horanvoj.music.domain.Album
import cz.fit.cvut.horanvoj.music.domain.Band
import cz.fit.cvut.horanvoj.music.domain.Track
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDateTime
import java.util.*

@SpringBootTest
class TrackServiceTest {
    @MockBean
    lateinit var albumRepository: AlbumRepository

    @MockBean
    lateinit var trackRepository: TrackRepository

    @Autowired
    lateinit var trackService: TrackService

    @Test
    fun `ensure reading a track works`() {
        val track = Track(1, 123, "Track one", listOf())

        Mockito
            .`when`(trackRepository.findById(track.id))
            .thenReturn(Optional.of(track))

        val trackFromService = trackService.findById(track.id)

        Assertions
            .assertTrue(trackFromService is Result.Success)
        Assertions
            .assertEquals(track, trackFromService.getOrThrow())
    }

    @Test
    fun `ensure removing a track works`() {
        var track = Track(1, 123, "Track one", listOf())
        val band = Band(1, "Band one", LocalDateTime.now(), null, null, listOf())
        val albumOne = Album(1, "Album one", LocalDateTime.now(), band, listOf(), listOf(track))
        val albumTwo = Album(2, "Album two", LocalDateTime.now(), band, listOf(), listOf(track))
        val albumOneNoTrack = albumOne.copy(tracks = listOf())
        val albumTwoNoTrack = albumTwo.copy(tracks = listOf())
        track = track.copy(albums = listOf(albumOne, albumTwo))

        Mockito
            .`when`(trackRepository.findById(track.id))
            .thenReturn(Optional.of(track))

        trackService.deleteById(track.id)

        Mockito
            .verify(albumRepository, times(1))
            .save(albumOneNoTrack)

        Mockito
            .verify(albumRepository, times(1))
            .save(albumTwoNoTrack)

        Mockito
            .verify(trackRepository, times(1))
            .deleteById(track.id)
    }

}