package cz.fit.cvut.horanvoj.music.rest

import cz.fit.cvut.horanvoj.music.business.BandService
import cz.fit.cvut.horanvoj.music.common.exception.EntityNameTooSimilarException
import cz.fit.cvut.horanvoj.music.common.extension.NoId
import cz.fit.cvut.horanvoj.music.domain.Band
import cz.fit.cvut.horanvoj.music.rest.controller.BandController
import cz.fit.cvut.horanvoj.music.rest.dto.band.BandDto
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.LocalDateTime

@SpringBootTest
class BandControllerIntegrationTest {
    @Autowired
    private lateinit var bandController: BandController

    @Autowired
    private lateinit var bandService: BandService

    @AfterEach
    fun tearDown() {
        bandService.deleteAll()
    }

    @Test
    fun `ensure band with similar name cannot be created`() {
        val band = Band(Long.NoId, "Band", LocalDateTime.now(), null, null, listOf())
        val bandTwo = BandDto(null, "Banda", LocalDateTime.now(), null, null, listOf())
        bandService.create(band)

        Assertions.assertThrows(EntityNameTooSimilarException::class.java) {
            bandController.addMapping(bandTwo)
        }
    }
}