package cz.fit.cvut.horanvoj.music.rest

import cz.fit.cvut.horanvoj.music.business.AlbumService
import cz.fit.cvut.horanvoj.music.business.BandService
import cz.fit.cvut.horanvoj.music.common.exception.NoSuchEntityFoundException
import cz.fit.cvut.horanvoj.music.common.getOrThrow
import cz.fit.cvut.horanvoj.music.domain.Album
import cz.fit.cvut.horanvoj.music.domain.Band
import cz.fit.cvut.horanvoj.music.domain.Track
import cz.fit.cvut.horanvoj.music.rest.controller.AlbumController
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@SpringBootTest
@Transactional
class AlbumControllerIntegrationTest {
    @Autowired
    private lateinit var albumController: AlbumController

    @Autowired
    private lateinit var albumService: AlbumService

    @Autowired
    private lateinit var bandService: BandService

    @AfterEach
    fun tearDown() {
        albumService.deleteAll()
        bandService.deleteAll()
    }

    @Test
    fun `ensure track cannot be removed from album when none is present`() {
        val band = Band(1, "Test band", LocalDateTime.now(), null, null, listOf())
        val track = Track(1, 123, "Track one", listOf())
        val album = Album(1, "Test album", LocalDateTime.now(), band, listOf(), listOf())

        val bandFromService = bandService.create(band).getOrThrow()
        val albumId = albumService.create(album.copy(band = bandFromService)).getOrThrow().id

        Assertions.assertThrows(NoSuchEntityFoundException::class.java) {
            albumController.removeTrack(albumId, track.id)
        }
    }
}