package cz.fit.cvut.horanvoj.music.unit

import cz.fit.cvut.horanvoj.music.domain.AlbumMediumHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class AlbumMediumHelperTest {

    @Test
    fun `ensure getFromLength generates different values for different digital qualities`() {
        val length = 100
        val mediumOne = AlbumMediumHelper.getFromLength(length, 240)
        val mediumTwo = AlbumMediumHelper.getFromLength(length, 1024)

        Assertions
            .assertNotEquals(mediumOne, mediumTwo)
    }
}