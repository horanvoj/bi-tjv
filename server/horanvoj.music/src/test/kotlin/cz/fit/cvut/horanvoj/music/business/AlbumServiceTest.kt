package cz.fit.cvut.horanvoj.music.business

import cz.fit.cvut.horanvoj.music.common.EntityExistsError
import cz.fit.cvut.horanvoj.music.common.Result
import cz.fit.cvut.horanvoj.music.dao.AlbumRepository
import cz.fit.cvut.horanvoj.music.dao.TrackRepository
import cz.fit.cvut.horanvoj.music.domain.Album
import cz.fit.cvut.horanvoj.music.domain.Band
import cz.fit.cvut.horanvoj.music.domain.Track
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.argThat
import org.mockito.kotlin.times
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDateTime
import java.util.*

@SpringBootTest
class AlbumServiceTest {
    @MockBean
    lateinit var albumRepository: AlbumRepository

    @MockBean
    lateinit var trackRepository: TrackRepository

    @Autowired
    lateinit var albumService: AlbumService

    @Test
    fun `ensure adding track to album works`() {
        val band = Band(1, "Band one", LocalDateTime.now(), null, null, listOf())
        val track = Track(1, 123, "Track one", listOf())
        val album = Album(1, "Album one", LocalDateTime.now(), band, listOf(), listOf())

        Mockito
            .`when`(trackRepository.findById(track.id))
            .thenReturn(Optional.of(track))

        Mockito
            .`when`(albumRepository.findById(album.id))
            .thenReturn(Optional.of(album))

        Mockito
            .`when`(albumRepository.save(argThat { id == album.id && tracks.contains(track) }))
            .thenReturn(album.copy(tracks = listOf(track)))

        val result = albumService.addTrack(album.id, track.id)

        Assertions
            .assertTrue(result is Result.Success)

        Assertions
            .assertTrue((result as Result.Success).data.tracks.contains(track))

        Mockito
            .verify(albumRepository, times(1))
            .findById(argThat { this == album.id })

        Mockito
            .verify(trackRepository, times(1))
            .findById(argThat { this == track.id })

        Mockito
            .verify(albumRepository, times(1))
            .save(argThat { id == album.id && tracks.contains(track) })
    }

    @Test
    fun `ensure adding track to album that already has it doesn't add anything`() {
        val band = Band(1, "Band one", LocalDateTime.now(), null, null, listOf())
        val track = Track(1, 123, "Track one", listOf())
        val album = Album(1, "Album one", LocalDateTime.now(), band, listOf(), listOf(track))

        Mockito
            .`when`(trackRepository.findById(track.id))
            .thenReturn(Optional.of(track.copy(albums = listOf(album))))

        Mockito
            .`when`(albumRepository.findById(album.id))
            .thenReturn(Optional.of(album))

        val result = albumService.addTrack(album.id, track.id)

        Assertions
            .assertTrue(result is Result.Error)

        Assertions
            .assertTrue((result as Result.Error).error is EntityExistsError)

        Mockito
            .verify(albumRepository, times(1))
            .findById(argThat { this == album.id })

        Mockito
            .verify(trackRepository, times(1))
            .findById(argThat { this == track.id })

        Mockito
            .verify(albumRepository, times(0))
            .save(argThat { id == album.id })
    }

    @Test
    fun `ensure removing track from album works`() {
        val band = Band(1, "Band one", LocalDateTime.now(), null, null, listOf())
        val track = Track(1, 123, "Track one", listOf())
        val album = Album(1, "Album one", LocalDateTime.now(), band, listOf(), listOf(track))

        Mockito
            .`when`(albumRepository.findById(album.id))
            .thenReturn(Optional.of(album))

        Mockito
            .`when`(albumRepository.save(argThat { id == album.id && !tracks.contains(track) }))
            .thenReturn(album.copy(tracks = listOf()))

        val result = albumService.removeTrack(album.id, track.id)

        Assertions
            .assertTrue(result is Result.Success)

        Assertions
            .assertFalse((result as Result.Success).data.tracks.contains(track))

        Mockito
            .verify(albumRepository, times(1))
            .findById(argThat { this == album.id })

        Mockito
            .verify(albumRepository, times(1))
            .save(argThat { id == album.id && !tracks.contains(track) })
    }

    @Test
    fun `ensure generating medium with different quality works`() {
        val band = Band(1, "Band one", LocalDateTime.now(), null, null, listOf())
        val track = Track(1, 100, "Track one", listOf())
        val album = Album(1, "Album one", LocalDateTime.now(), band, listOf(), listOf(track))

        Mockito
            .`when`(albumRepository.findById(album.id))
            .thenReturn(Optional.of(album))

        val resultOne = albumService.getMediums(album.id, digitalQuality = 128)
        val resultTwo = albumService.getMediums(album.id, digitalQuality = 320)

        Assertions
            .assertTrue(resultOne is Result.Success)
        Assertions
            .assertTrue(resultTwo is Result.Success)

        val mediumsOne = (resultOne as Result.Success).data
        val mediumsTwo = (resultTwo as Result.Success).data

        Assertions
            .assertNotEquals(mediumsOne.size, mediumsTwo.size)
    }
}