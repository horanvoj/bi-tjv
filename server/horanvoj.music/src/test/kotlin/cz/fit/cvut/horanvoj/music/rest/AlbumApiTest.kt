package cz.fit.cvut.horanvoj.music.rest

import cz.fit.cvut.horanvoj.music.business.AlbumService
import cz.fit.cvut.horanvoj.music.common.EntityNotFoundError
import cz.fit.cvut.horanvoj.music.common.Result
import cz.fit.cvut.horanvoj.music.domain.Album
import cz.fit.cvut.horanvoj.music.domain.Band
import cz.fit.cvut.horanvoj.music.domain.Track
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDateTime

@SpringBootTest
@AutoConfigureMockMvc
class AlbumApiTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var albumService: AlbumService

    @Test
    fun `ensure track can be added to album`() {
        val band = Band(1, "Band one", LocalDateTime.now(), null, null, listOf())
        val track = Track(1, 100, "Track one", listOf())
        val album = Album(1, "Album one", LocalDateTime.now(), band, listOf(), listOf())

        Mockito
            .`when`(albumService.addTrack(album.id, track.id))
            .thenReturn(Result.Success(album.copy(tracks = listOf(track))))

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .put("/album/${album.id}/track/${track.id}")
                    .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `ensure adding a non existent track reports correct error`() {
        val band = Band(1, "Band one", LocalDateTime.now(), null, null, listOf())
        val album = Album(1, "Album one", LocalDateTime.now(), band, listOf(), listOf())

        Mockito
            .`when`(albumService.addTrack(anyLong(), anyLong()))
            .thenReturn(Result.Error(EntityNotFoundError))

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .put("/album/${album.id}/track/1")
                    .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(MockMvcResultMatchers.status().isNotFound)
    }

}