package cz.fit.cvut.horanvoj.music.dao

import cz.fit.cvut.horanvoj.music.common.extension.NoId
import cz.fit.cvut.horanvoj.music.domain.Band
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.time.LocalDateTime

@DataJpaTest
@AutoConfigureTestDatabase(
    replace = AutoConfigureTestDatabase.Replace.NONE // Testing a custom query available on postgres only
)
class BandRepositoryTest {

    @Autowired
    lateinit var bandRepository: BandRepository

    @AfterEach
    fun tearDown() {
        bandRepository.deleteAll()
    }

    @Test
    fun `ensure similar name detection works`() {
        val bandOne = Band(Long.NoId, "Van Halen", LocalDateTime.now(), null, null, listOf())
        val bandTwo = Band(Long.NoId, "Fan Halen", LocalDateTime.now(), null, null, listOf())

        val original = bandRepository.save(bandOne)
        val similar = bandRepository.findBySimilarName(bandTwo.name)

        Assertions
            .assertTrue(similar.isPresent)
        Assertions
            .assertEquals(original, similar.get())

    }
}