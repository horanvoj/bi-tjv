package cz.fit.cvut.horanvoj.music.rest.dto.release

import cz.fit.cvut.horanvoj.music.rest.dto.DtoModel
import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDateTime

@Schema(name = "Release")
data class ReleaseDto(
    override val id: Long?,
    val date: LocalDateTime,
    val type: ReleaseTypeDto,
    val name: String?,
    val albumId: Long,
) : DtoModel<Long, ReleaseDto> {
    override fun withId(id: Long?) = copy(id = id)
}
