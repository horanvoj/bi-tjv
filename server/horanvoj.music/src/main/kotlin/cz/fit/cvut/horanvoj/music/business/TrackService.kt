package cz.fit.cvut.horanvoj.music.business

import cz.fit.cvut.horanvoj.music.common.getOrThrow
import cz.fit.cvut.horanvoj.music.dao.AlbumRepository
import cz.fit.cvut.horanvoj.music.dao.TrackRepository
import cz.fit.cvut.horanvoj.music.domain.Track
import org.springframework.stereotype.Component

@Component
class TrackService(
    private val albumRepository: AlbumRepository,
    private val trackRepository: TrackRepository,
) : AbstractCrudService<Track, Long>(trackRepository) {
    // Also Delete track from all its albums
    override fun deleteById(id: Long) {
        findById(id)
            .getOrThrow()
            .albums
            .map { it.copy(tracks = it.tracks.filterNot { track -> track.id == id }) }
            .forEach { albumRepository.save(it) }
            .also { trackRepository.deleteById(id) }

    }
}