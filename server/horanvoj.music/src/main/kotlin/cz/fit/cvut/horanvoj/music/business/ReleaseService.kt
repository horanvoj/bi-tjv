package cz.fit.cvut.horanvoj.music.business

import cz.fit.cvut.horanvoj.music.common.BadParamsError
import cz.fit.cvut.horanvoj.music.common.Result
import cz.fit.cvut.horanvoj.music.dao.ReleaseRepository
import cz.fit.cvut.horanvoj.music.domain.Release
import org.springframework.stereotype.Component

@Component
class ReleaseService(
    releaseRepository: ReleaseRepository,
) : AbstractCrudService<Release, Long>(releaseRepository) {
    override fun createConstraints(entity: Release): Result<Unit> {
        if (entity.album.releaseDate > entity.date) {
            return Result.Error(BadParamsError("Release cannot be created before album was released"))
        }
        return super.createConstraints(entity)
    }
}