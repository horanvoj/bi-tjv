package cz.fit.cvut.horanvoj.music.domain

import org.hibernate.Hibernate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
data class Track(
    @Id
    @GeneratedValue
    override val id: Long,
    val length: Int,
    val name: String,
    @ManyToMany(mappedBy = "tracks")
    val albums: List<Album>
) : DomainEntity<Long> {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Track

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return super.toString()
    }
}
