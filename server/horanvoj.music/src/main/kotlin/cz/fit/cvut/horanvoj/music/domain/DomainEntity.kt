package cz.fit.cvut.horanvoj.music.domain

interface DomainEntity<Key : Any> {
    val id: Key
}
