package cz.fit.cvut.horanvoj.music.common

import java.util.*

sealed interface Result<out T : Any> {
    data class Success<out T : Any>(val data: T) : Result<T>
    data class Error(val error: ErrorResult) : Result<Nothing>
}

fun <T : Any?> T.toResult() = when (this) {
    null -> Result.Error(ObjectNullError)
    else -> Result.Success(this)
}

inline fun <T : Any, E : Any> Result<T>.map(transform: (T) -> E): Result<E> =
    when (this) {
        is Result.Error -> this
        is Result.Success -> Result.Success(transform(data))
    }

inline fun <T : Any> Result<T>.getOrAbort(onAbort: (Result.Error) -> Unit) =
    when (this) {
        is Result.Success -> data
        is Result.Error -> {
            onAbort(this)
            throw IllegalStateException()
        }
    }

fun <T : Any> Result<T>.getOrThrow(): T =
    when (this) {
        is Result.Error -> throw this.error.exception
        is Result.Success -> this.data
    }

fun <T> Optional<T>.getOrThrow(): T = orElseThrow()

inline fun <T> Optional<T>.getOrAbort(onAbort: (Result.Error) -> Unit): T =
    if (isPresent) {
        get()
    } else {
        onAbort(Result.Error(EntityNotFoundError))
        throw IllegalStateException()
    }