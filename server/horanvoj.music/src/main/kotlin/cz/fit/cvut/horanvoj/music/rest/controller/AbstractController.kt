package cz.fit.cvut.horanvoj.music.rest.controller

import cz.fit.cvut.horanvoj.music.common.Result
import cz.fit.cvut.horanvoj.music.common.getOrThrow
import cz.fit.cvut.horanvoj.music.common.map
import cz.fit.cvut.horanvoj.music.rest.dto.DtoMapper
import cz.fit.cvut.horanvoj.music.rest.dto.DtoModel
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.web.bind.annotation.*

abstract class AbstractController<Domain : Any, Dto : DtoModel<Long, Dto>>(
    private val mapper: DtoMapper<Domain, Dto, Long>,
) {
    protected abstract fun getAll(): List<Domain>

    @GetMapping
    @ResponseBody
    @Operation(
        responses = [
            ApiResponse(responseCode = "200"),
        ],
        summary = "Get all entities from this collection"
    )
    fun getAllMapping(): List<Dto> = getAll().map { it.toDto() }

    protected abstract fun getById(id: Long): Result<Domain>

    @GetMapping("/{id}")
    @ResponseBody
    @Operation(
        responses = [
            ApiResponse(responseCode = "200"),
            ApiResponse(
                responseCode = "404",
                content = [Content()],
                description = "Entity with id was not found in this collection"
            ),
        ],
        summary = "Get entity by id from this collection"
    )
    fun getByIdMapping(@PathVariable id: Long): Dto = getById(id).toDto()

    protected abstract fun add(domain: Domain): Result<Domain>

    @PostMapping
    @ResponseBody
    @Operation(
        responses = [
            ApiResponse(responseCode = "200"),
            ApiResponse(responseCode = "400", content = [Content()], description = "Invalid body")
        ],
        summary = "Add an entity to this collection"
    )
    fun addMapping(@RequestBody dto: Dto): Dto {
        return add(dto.withId(null).toDomain()).toDto()
    }

    protected abstract fun update(domain: Domain): Result<Domain>

    @PutMapping("/{id}")
    @ResponseBody
    @Operation(
        responses = [
            ApiResponse(responseCode = "200"),
            ApiResponse(responseCode = "400", content = [Content()], description = "Invalid body"),
            ApiResponse(
                responseCode = "404",
                content = [Content()],
                description = "Entity with id was not found in this collection"
            ),
        ],
        summary = "Edit an entity in this collection"
    )
    fun updateMapping(@PathVariable id: Long, @RequestBody dto: Dto): Dto {
        return update(dto.withId(id).toDomain()).toDto()
    }

    protected abstract fun delete(id: Long)

    @DeleteMapping("/{id}")
    @ResponseBody
    @Operation(
        responses = [
            ApiResponse(responseCode = "200"),
            ApiResponse(
                responseCode = "404",
                content = [Content()],
                description = "Entity with id was not found in this collection"
            )
        ],
        summary = "Delete an entity from this collection"
    )
    fun deleteMapping(@PathVariable id: Long) = delete(id)

    protected fun Dto.toDomain() = with(mapper) { toDomain() }
    protected fun Domain.toDto() = with(mapper) { toDto() }

    protected fun Result<Domain>.toDto() = map { it.toDto() }.getOrThrow()
}