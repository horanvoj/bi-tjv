package cz.fit.cvut.horanvoj.music.rest.dto.release

import cz.fit.cvut.horanvoj.music.business.AlbumService
import cz.fit.cvut.horanvoj.music.common.extension.orNoId
import cz.fit.cvut.horanvoj.music.common.getOrThrow
import cz.fit.cvut.horanvoj.music.domain.Release
import cz.fit.cvut.horanvoj.music.rest.dto.DtoMapper
import org.springframework.stereotype.Component

@Component
class ReleaseDtoMapper(
    private val albumService: AlbumService,
) : DtoMapper<Release, ReleaseDto, Long> {
    override fun Release.toDto() = ReleaseDto(
        id = id,
        date = date,
        type = type.toDto(),
        name = name,
        albumId = album.id,
    )

    override fun ReleaseDto.toDomain() = Release(
        id = id.orNoId(),
        date = date,
        type = type.toDomain(),
        name = name,
        album = albumService.findById(albumId).getOrThrow()
    )
}