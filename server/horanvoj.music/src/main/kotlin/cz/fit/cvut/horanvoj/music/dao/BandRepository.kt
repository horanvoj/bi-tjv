package cz.fit.cvut.horanvoj.music.dao

import cz.fit.cvut.horanvoj.music.domain.Band
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface BandRepository : CrudRepository<Band, Long> {
    @Query("select b from Band b where levenshtein_less_equal(b.name, ?1, 2) <= 2")
    fun findBySimilarName(name: String): Optional<Band>
}