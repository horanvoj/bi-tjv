package cz.fit.cvut.horanvoj.music.common.exception

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class EntityNameTooSimilarException(
    similarEntityName: String
) : ResponseStatusException(
    HttpStatus.CONFLICT,
    "Entity with a similar name already exists: $similarEntityName",
)