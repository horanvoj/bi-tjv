package cz.fit.cvut.horanvoj.music.common.extension

val Long.Companion.NoId get() = -1L

fun Long?.orNoId(): Long = this ?: Long.NoId