package cz.fit.cvut.horanvoj.music.domain

enum class ReleaseType {
    LP, CD, Digital, Cassette,
}