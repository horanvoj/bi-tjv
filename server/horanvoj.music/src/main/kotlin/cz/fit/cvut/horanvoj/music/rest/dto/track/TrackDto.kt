package cz.fit.cvut.horanvoj.music.rest.dto.track

import cz.fit.cvut.horanvoj.music.rest.dto.DtoModel
import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "Track")
data class TrackDto(
    override val id: Long?,
    val length: Int,
    val name: String,
) : DtoModel<Long, TrackDto> {
    override fun withId(id: Long?) = copy(id = id)
}