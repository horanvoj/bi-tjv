package cz.fit.cvut.horanvoj.music.rest.dto.album

import cz.fit.cvut.horanvoj.music.business.BandService
import cz.fit.cvut.horanvoj.music.common.extension.orNoId
import cz.fit.cvut.horanvoj.music.common.getOrThrow
import cz.fit.cvut.horanvoj.music.domain.Album
import cz.fit.cvut.horanvoj.music.rest.dto.DtoMapper
import cz.fit.cvut.horanvoj.music.rest.dto.release.ReleaseDtoMapper
import cz.fit.cvut.horanvoj.music.rest.dto.track.TrackDtoMapper
import org.springframework.stereotype.Component

@Component
class AlbumDtoMapper(
    private val releaseDtoMapper: ReleaseDtoMapper,
    private val trackDtoMapper: TrackDtoMapper,
    private val bandService: BandService,
) : DtoMapper<Album, AlbumDto, Long> {
    override fun Album.toDto() = AlbumDto(
        id = id,
        name = name,
        releaseDate = releaseDate,
        bandId = band.id,
        releases = with(releaseDtoMapper) { releases.map { it.toDto() } },
        tracks = with(trackDtoMapper) { tracks.map { it.toDto() } },
    )

    override fun AlbumDto.toDomain() = Album(
        id = id.orNoId(),
        name = name,
        releaseDate = releaseDate,
        band = bandService.findById(bandId).getOrThrow(),
        releases = with(releaseDtoMapper) { releases.map { it.toDomain() } },
        tracks = with(trackDtoMapper) { tracks.map { it.toDomain() } },
    )
}