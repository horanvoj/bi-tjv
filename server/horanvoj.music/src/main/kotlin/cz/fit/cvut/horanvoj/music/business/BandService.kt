package cz.fit.cvut.horanvoj.music.business

import cz.fit.cvut.horanvoj.music.common.BadParamsError
import cz.fit.cvut.horanvoj.music.common.EntityNameTooSimilarError
import cz.fit.cvut.horanvoj.music.common.Result
import cz.fit.cvut.horanvoj.music.dao.BandRepository
import cz.fit.cvut.horanvoj.music.domain.Band
import org.springframework.stereotype.Component

@Component
class BandService(
    private val bandRepository: BandRepository,
) : AbstractCrudService<Band, Long>(bandRepository) {
    override fun createConstraints(entity: Band): Result<Unit> {
        if (entity.endDate != null && entity.endDate < entity.creationDate) {
            return Result.Error(BadParamsError("Band cannot end before its creation"))
        }
        val similar = bandRepository.findBySimilarName(entity.name)
        if (similar.isPresent && similar.get().id != entity.id) {
            return Result.Error(EntityNameTooSimilarError(similar.get().name))
        }
        return super.createConstraints(entity)
    }
}