package cz.fit.cvut.horanvoj.music.business

import cz.fit.cvut.horanvoj.music.common.*
import cz.fit.cvut.horanvoj.music.domain.DomainEntity
import org.springframework.data.repository.CrudRepository

abstract class AbstractCrudService<Entity, Key>(
    private val repository: CrudRepository<Entity, Key>,
) where Entity : DomainEntity<Key>, Key : Any {

    /**
     * Constraints used when creating & updating entities
     */
    open fun createConstraints(entity: Entity): Result<Unit> {
        return Result.Success(Unit)
    }

    open fun create(entity: Entity): Result<Entity> {
        createConstraints(entity)
            .getOrAbort { return it }
        if (repository.existsById(entity.id)) {
            return Result.Error(EntityExistsError)
        }
        return repository.save(entity).toResult()
    }

    open fun findById(id: Key): Result<Entity> {
        val entity = repository.findById(id)
        if (entity.isEmpty) {
            return Result.Error(EntityNotFoundError)
        }
        return entity.get().toResult()
    }

    fun readAll(): List<Entity> = repository.findAll().toList()

    open fun update(entity: Entity): Result<Entity> {
        createConstraints(entity)
            .getOrAbort { return it }
        if (!repository.existsById(entity.id)) {
            return Result.Error(EntityNotFoundError)
        }
        return repository.save(entity).toResult()
    }

    open fun deleteById(id: Key) = repository.deleteById(id)

    open fun deleteAll() = repository.deleteAll()
}