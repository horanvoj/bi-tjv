package cz.fit.cvut.horanvoj.music.common

import cz.fit.cvut.horanvoj.music.common.exception.BadParamsException
import cz.fit.cvut.horanvoj.music.common.exception.EntityExistsException
import cz.fit.cvut.horanvoj.music.common.exception.EntityNameTooSimilarException
import cz.fit.cvut.horanvoj.music.common.exception.NoSuchEntityFoundException

sealed interface ErrorResult {
    val exception: Exception
}

object ObjectNullError : ErrorResult {
    override val exception: Exception = NoSuchEntityFoundException()
}

object EntityExistsError : ErrorResult {
    override val exception: Exception = EntityExistsException()
}

object EntityNotFoundError : ErrorResult {
    override val exception: Exception = NoSuchEntityFoundException()
}

data class BadParamsError(val reason: String) : ErrorResult {
    override val exception: Exception = BadParamsException(reason)
}

class EntityNameTooSimilarError(similarEntityName: String) : ErrorResult {
    override val exception: Exception = EntityNameTooSimilarException(similarEntityName)
}