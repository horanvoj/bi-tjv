package cz.fit.cvut.horanvoj.music.rest.dto.release

import cz.fit.cvut.horanvoj.music.domain.ReleaseType

enum class ReleaseTypeDto {
    LP, CD, Digital, Cassette,
}

fun ReleaseType.toDto() = when (this) {
    ReleaseType.LP -> ReleaseTypeDto.LP
    ReleaseType.CD -> ReleaseTypeDto.CD
    ReleaseType.Digital -> ReleaseTypeDto.Digital
    ReleaseType.Cassette -> ReleaseTypeDto.Cassette
}

fun ReleaseTypeDto.toDomain() = when (this) {
    ReleaseTypeDto.LP -> ReleaseType.LP
    ReleaseTypeDto.CD -> ReleaseType.CD
    ReleaseTypeDto.Digital -> ReleaseType.Digital
    ReleaseTypeDto.Cassette -> ReleaseType.Cassette
}