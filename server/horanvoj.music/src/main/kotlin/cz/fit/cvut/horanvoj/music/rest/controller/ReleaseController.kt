package cz.fit.cvut.horanvoj.music.rest.controller

import cz.fit.cvut.horanvoj.music.business.ReleaseService
import cz.fit.cvut.horanvoj.music.domain.Release
import cz.fit.cvut.horanvoj.music.rest.dto.release.ReleaseDto
import cz.fit.cvut.horanvoj.music.rest.dto.release.ReleaseDtoMapper
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/release")
@Tag(name = "Release")
class ReleaseController internal constructor(
    private val releaseService: ReleaseService,
    releaseDtoMapper: ReleaseDtoMapper,
) : AbstractController<Release, ReleaseDto>(releaseDtoMapper) {
    override fun getAll() = releaseService.readAll()

    override fun getById(id: Long) = releaseService.findById(id)

    override fun add(domain: Release) = releaseService.create(domain)

    override fun update(domain: Release) = releaseService.update(domain)

    override fun delete(id: Long) = releaseService.deleteById(id)
}