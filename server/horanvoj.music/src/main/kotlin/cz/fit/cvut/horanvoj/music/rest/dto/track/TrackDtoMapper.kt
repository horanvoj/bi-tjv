package cz.fit.cvut.horanvoj.music.rest.dto.track

import cz.fit.cvut.horanvoj.music.business.TrackService
import cz.fit.cvut.horanvoj.music.common.extension.orNoId
import cz.fit.cvut.horanvoj.music.common.getOrThrow
import cz.fit.cvut.horanvoj.music.domain.Track
import cz.fit.cvut.horanvoj.music.rest.dto.DtoMapper
import org.springframework.stereotype.Component

@Component
class TrackDtoMapper(
    private val trackService: TrackService,
) : DtoMapper<Track, TrackDto, Long> {
    override fun Track.toDto() = TrackDto(
        id = id,
        length = length,
        name = name,
    )

    override fun TrackDto.toDomain() = Track(
        id = id.orNoId(),
        length = length,
        name = name,
        albums = id?.let {
            trackService
                .findById(id)
                .getOrThrow()
                .albums
        } ?: emptyList()
    )
}