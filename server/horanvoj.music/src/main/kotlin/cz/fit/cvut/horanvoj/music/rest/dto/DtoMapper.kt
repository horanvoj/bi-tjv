package cz.fit.cvut.horanvoj.music.rest.dto

interface DtoMapper<Domain : Any, Dto : DtoModel<Key, Dto>, Key : Any> {
    fun Domain.toDto(): Dto
    fun Dto.toDomain(): Domain
}