package cz.fit.cvut.horanvoj.music.rest.dto.error

import org.springframework.http.HttpStatus

class ErrorResponse private constructor(
    val code: Int,
    val status: String,
    val message: String?,
) {
    constructor(
        httpStatus: HttpStatus,
        message: String? = null,
    ) : this(
        code = httpStatus.value(),
        status = httpStatus.name,
        message = message
    )
}