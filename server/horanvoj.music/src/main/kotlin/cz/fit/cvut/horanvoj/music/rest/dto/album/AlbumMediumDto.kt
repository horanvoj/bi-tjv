package cz.fit.cvut.horanvoj.music.rest.dto.album

import cz.fit.cvut.horanvoj.music.domain.AlbumMedium
import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "Album medium")
data class AlbumMediumDto(
    @field:Schema(description = "Number of LP discs")
    val records: Int,
    @field:Schema(description = "Number of CD disc")
    val discs: Int,
    @field:Schema(description = "Size in bytes")
    val size: Long,
    @field:Schema(description = "Number of cassette tapes")
    val tapes: Int,
)

fun AlbumMedium.toDto() = AlbumMediumDto(
    records = records,
    discs = discs,
    size = size,
    tapes = tapes
)