package cz.fit.cvut.horanvoj.music.rest.controller

import cz.fit.cvut.horanvoj.music.business.AlbumService
import cz.fit.cvut.horanvoj.music.common.getOrThrow
import cz.fit.cvut.horanvoj.music.domain.Album
import cz.fit.cvut.horanvoj.music.rest.dto.album.AlbumDto
import cz.fit.cvut.horanvoj.music.rest.dto.album.AlbumDtoMapper
import cz.fit.cvut.horanvoj.music.rest.dto.album.AlbumMediumDto
import cz.fit.cvut.horanvoj.music.rest.dto.album.toDto
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/album")
@Tag(name = "Album")
class AlbumController internal constructor(
    private val albumService: AlbumService,
    albumDtoMapper: AlbumDtoMapper,
) : AbstractController<Album, AlbumDto>(albumDtoMapper) {
    override fun getAll() = albumService.readAll()

    override fun getById(id: Long) = albumService.findById(id)

    override fun delete(id: Long) = albumService.deleteById(id)

    override fun update(domain: Album) = albumService.update(domain)

    override fun add(domain: Album) = albumService.create(domain)

    @PutMapping("{id}/track/{trackId}")
    @ResponseBody
    @Operation(
        responses = [
            ApiResponse(responseCode = "200"),
            ApiResponse(
                responseCode = "404",
                content = [Content()],
                description = "Album or track with id was not found"
            ),
            ApiResponse(responseCode = "409", content = [Content()], description = "Track was already added to album"),
        ],
        summary = "Add a track to album"
    )
    fun addTrack(
        @PathVariable
        id: Long,
        @PathVariable
        trackId: Long
    ): AlbumDto = albumService
        .addTrack(id, trackId)
        .toDto()

    @DeleteMapping("{id}/track/{trackId}")
    @ResponseBody
    @Operation(
        responses = [
            ApiResponse(responseCode = "200"),
            ApiResponse(
                responseCode = "404",
                content = [Content()],
                description = "Album or track with id was not found"
            ),
        ],
        summary = "Remove a track from album"
    )
    fun removeTrack(
        @PathVariable
        id: Long,
        @PathVariable
        trackId: Long
    ): AlbumDto = albumService
        .removeTrack(id, trackId)
        .toDto()

    @GetMapping("{id}/medium")
    @ResponseBody
    @Operation(
        responses = [
            ApiResponse(responseCode = "200"),
            ApiResponse(responseCode = "404", content = [Content()], description = "Album with id was not found"),
        ],
        summary = "Get information about the album medium"
    )
    fun getMediums(
        @PathVariable
        id: Long,
        @RequestParam(required = false)
        digitalQuality: Long?,
    ): AlbumMediumDto = albumService
        .getMediums(id, digitalQuality)
        .getOrThrow()
        .toDto()
}