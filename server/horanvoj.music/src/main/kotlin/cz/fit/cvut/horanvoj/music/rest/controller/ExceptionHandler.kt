package cz.fit.cvut.horanvoj.music.rest.controller

import cz.fit.cvut.horanvoj.music.rest.dto.error.ErrorResponse
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {
    fun ResponseStatusException.toErrorResponseEntity() =
        ResponseEntity
            .status(status)
            .body(
                ErrorResponse(status, reason)
            )

    @ExceptionHandler
    fun handleException(
        e: ResponseStatusException
    ): ResponseEntity<ErrorResponse> {
        return e.toErrorResponseEntity()
    }

    override fun handleExceptionInternal(
        ex: java.lang.Exception,
        body: Any?,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        super.handleExceptionInternal(ex, body, headers, status, request)
        return ResponseEntity
            .status(status)
            .body(
                ErrorResponse(status, ex.message)
            )
    }

    @ExceptionHandler
    fun handleException(
        e: JpaObjectRetrievalFailureException
    ): ResponseEntity<ErrorResponse> {
        return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .body(
                ErrorResponse(
                    HttpStatus.BAD_REQUEST,
                )
            )
    }

    @ExceptionHandler
    fun handleException(
        e: Exception
    ): ResponseEntity<ErrorResponse> {
        return ResponseEntity
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(
                ErrorResponse(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                )
            )
    }

}