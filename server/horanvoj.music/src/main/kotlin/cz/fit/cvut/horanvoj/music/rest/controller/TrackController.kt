package cz.fit.cvut.horanvoj.music.rest.controller

import cz.fit.cvut.horanvoj.music.business.TrackService
import cz.fit.cvut.horanvoj.music.domain.Track
import cz.fit.cvut.horanvoj.music.rest.dto.track.TrackDto
import cz.fit.cvut.horanvoj.music.rest.dto.track.TrackDtoMapper
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/track")
@Tag(name = "Track")
class TrackController internal constructor(
    private val trackService: TrackService,
    trackDtoMapper: TrackDtoMapper,
) : AbstractController<Track, TrackDto>(trackDtoMapper) {
    override fun getAll() = trackService.readAll()

    override fun getById(id: Long) = trackService.findById(id)

    override fun add(domain: Track) = trackService.create(domain)

    override fun update(domain: Track) = trackService.update(domain)

    override fun delete(id: Long) = trackService.deleteById(id)
}