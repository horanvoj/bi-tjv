package cz.fit.cvut.horanvoj.music.dao

import cz.fit.cvut.horanvoj.music.domain.Release
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ReleaseRepository : CrudRepository<Release, Long>