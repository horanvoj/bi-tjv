package cz.fit.cvut.horanvoj.music.rest.controller

import cz.fit.cvut.horanvoj.music.business.BandService
import cz.fit.cvut.horanvoj.music.domain.Band
import cz.fit.cvut.horanvoj.music.rest.dto.band.BandDto
import cz.fit.cvut.horanvoj.music.rest.dto.band.BandDtoMapper
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/band")
@Tag(name = "Band")
class BandController internal constructor(
    private val bandService: BandService,
    bandDtoMapper: BandDtoMapper
) : AbstractController<Band, BandDto>(bandDtoMapper) {
    override fun getAll() = bandService.readAll()

    override fun getById(id: Long) = bandService.findById(id)

    override fun delete(id: Long) = bandService.deleteById(id)

    override fun update(domain: Band) = bandService.update(domain)

    override fun add(domain: Band) = bandService.create(domain)
}