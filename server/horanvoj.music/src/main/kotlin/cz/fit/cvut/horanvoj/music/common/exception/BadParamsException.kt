package cz.fit.cvut.horanvoj.music.common.exception

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class BadParamsException(reason: String) : ResponseStatusException(
    HttpStatus.BAD_REQUEST,
    "Bad parameters: $reason",
)