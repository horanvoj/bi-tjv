package cz.fit.cvut.horanvoj.music.domain

import org.hibernate.Hibernate
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Album(
    @Id
    @GeneratedValue
    override val id: Long,
    val name: String,
    val releaseDate: LocalDateTime,
    @ManyToOne(fetch = FetchType.LAZY)
    val band: Band,
    @OneToMany(mappedBy = "album", cascade = [CascadeType.ALL])
    val releases: List<Release>,
    @ManyToMany
    val tracks: List<Track>,
) : DomainEntity<Long> {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Album

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return super.toString()
    }
}
