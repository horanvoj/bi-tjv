package cz.fit.cvut.horanvoj.music.domain

import org.hibernate.Hibernate
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Band(
    @Id
    @GeneratedValue
    override val id: Long,
    val name: String,
    val creationDate: LocalDateTime,
    val endDate: LocalDateTime?,
    @Column(length = 2048)
    val bio: String?,
    @OneToMany(mappedBy = "band", cascade = [CascadeType.ALL])
    val albums: List<Album>,
    val imageUrl: String?,
) : DomainEntity<Long> {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Band

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return super.toString()
    }
}