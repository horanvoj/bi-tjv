package cz.fit.cvut.horanvoj.music.rest.dto.band

import cz.fit.cvut.horanvoj.music.rest.dto.DtoModel
import cz.fit.cvut.horanvoj.music.rest.dto.album.AlbumDto
import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDateTime

@Schema(name = "Band")
data class BandDto(
    override val id: Long?,
    val name: String,
    val creationDate: LocalDateTime,
    val endDate: LocalDateTime?,
    val bio: String?,
    val albums: List<AlbumDto>,
    val imageUrl: String?,
) : DtoModel<Long, BandDto> {
    override fun withId(id: Long?) = copy(id = id)
}
