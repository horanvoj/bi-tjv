package cz.fit.cvut.horanvoj.music.rest.dto

interface DtoModel<Key : Any, T : DtoModel<Key, T>> {
    val id: Key?

    fun withId(id: Key?): T
}