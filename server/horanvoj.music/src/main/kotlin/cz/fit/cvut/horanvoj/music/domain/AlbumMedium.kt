package cz.fit.cvut.horanvoj.music.domain

import kotlin.math.ceil

data class AlbumMedium(
    val records: Int,
    val discs: Int,
    val size: Long,
    val tapes: Int,
)

object AlbumMediumHelper {
    fun getFromLength(
        length: Int,
        digitalQuality: Long? = DefaultDigitalQuality
    ) = AlbumMedium(
        records = countMediums(LPMinutes, length),
        discs = countMediums(CDMinutes, length),
        size = countSize(digitalQuality ?: DefaultDigitalQuality, length),
        tapes = countMediums(CassetteMinutes, length),
    )

    private fun countSize(quality: Long, length: Int) = ((quality * 1000 * length) / 8L) * 2

    private fun countMediums(
        minutesPerOneMedium: Int,
        length: Int,
    ): Int {
        return ceil(length.toFloat() / (minutesPerOneMedium * 60)).toInt()
    }

    private const val CassetteMinutes = 60
    private const val CDMinutes = 74
    private const val LPMinutes = 44
    private const val DefaultDigitalQuality = 320L // 320 Kbps
}
