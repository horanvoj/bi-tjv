package cz.fit.cvut.horanvoj.music.rest.dto.album

import cz.fit.cvut.horanvoj.music.rest.dto.DtoModel
import cz.fit.cvut.horanvoj.music.rest.dto.release.ReleaseDto
import cz.fit.cvut.horanvoj.music.rest.dto.track.TrackDto
import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDateTime

@Schema(name = "Album")
data class AlbumDto(
    override val id: Long?,
    val name: String,
    val releaseDate: LocalDateTime,
    val bandId: Long,
    val releases: List<ReleaseDto>,
    val tracks: List<TrackDto>,
) : DtoModel<Long, AlbumDto> {
    override fun withId(id: Long?) = copy(id = id)
}