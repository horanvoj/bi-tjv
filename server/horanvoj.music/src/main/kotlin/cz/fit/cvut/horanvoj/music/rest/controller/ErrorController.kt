package cz.fit.cvut.horanvoj.music.rest.controller

import io.swagger.v3.oas.annotations.Hidden
import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Hidden
@RestController
class ErrorController : ErrorController {
    @RequestMapping("error/")
    fun error(): String {
        return ""
    }
}