package cz.fit.cvut.horanvoj.music.dao

import cz.fit.cvut.horanvoj.music.domain.Track
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TrackRepository : CrudRepository<Track, Long>