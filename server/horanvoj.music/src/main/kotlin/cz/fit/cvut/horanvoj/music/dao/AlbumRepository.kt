package cz.fit.cvut.horanvoj.music.dao

import cz.fit.cvut.horanvoj.music.domain.Album
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AlbumRepository : CrudRepository<Album, Long> {
    @Query("select a from Album a where levenshtein_less_equal(a.name, ?1, 2) <= 2")
    fun findBySimilarName(name: String): Optional<Album>
}