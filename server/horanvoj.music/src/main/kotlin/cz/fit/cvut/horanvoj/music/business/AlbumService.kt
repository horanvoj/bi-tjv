package cz.fit.cvut.horanvoj.music.business

import cz.fit.cvut.horanvoj.music.common.*
import cz.fit.cvut.horanvoj.music.dao.AlbumRepository
import cz.fit.cvut.horanvoj.music.dao.TrackRepository
import cz.fit.cvut.horanvoj.music.domain.Album
import cz.fit.cvut.horanvoj.music.domain.AlbumMedium
import cz.fit.cvut.horanvoj.music.domain.AlbumMediumHelper
import org.springframework.stereotype.Component

@Component
class AlbumService(
    private val albumRepository: AlbumRepository,
    private val trackRepository: TrackRepository,
) : AbstractCrudService<Album, Long>(albumRepository) {
    override fun createConstraints(entity: Album): Result<Unit> {
        if (entity.band.creationDate > entity.releaseDate) {
            return Result.Error(BadParamsError("Album cannot be released before bands creation"))
        }
        val similar = albumRepository.findBySimilarName(entity.name)
        if (similar.isPresent && similar.get().id != entity.id) {
            return Result.Error(EntityNameTooSimilarError(similar.get().name))
        }
        return super.createConstraints(entity)
    }

    private fun updateAlbum(
        id: Long,
        update: (album: Album) -> Result<Album>
    ): Result<Album> {
        return findById(id)
            .getOrAbort {
                return Result.Error(EntityNotFoundError)
            }
            .let(update)
            .getOrAbort { return it }
            .let {
                albumRepository.save(it).toResult()
            }
    }

    fun addTrack(id: Long, trackId: Long): Result<Album> {
        return updateAlbum(id) { album ->
            trackRepository
                .findById(trackId)
                .getOrAbort {
                    return@updateAlbum it
                }
                .let { track ->
                    if (album.tracks.contains(track)) {
                        return@updateAlbum Result.Error(EntityExistsError)
                    }
                    album.copy(tracks = album.tracks + track)
                }
                .toResult()
        }
    }

    fun removeTrack(id: Long, trackId: Long): Result<Album> {
        return updateAlbum(id) { album ->
            album.tracks
                .filterNot { it.id == trackId }
                .let { tracks ->
                    if (tracks.size == album.tracks.size) {
                        return@updateAlbum Result.Error(EntityNotFoundError)
                    }
                    album.copy(tracks = tracks)
                }
                .toResult()
        }
    }

    fun getMediums(
        id: Long,
        digitalQuality: Long?
    ): Result<AlbumMedium> {
        return findById(id).map { album ->
            album.tracks
                .sumOf { track -> track.length }
                .let { totalLength ->
                    AlbumMediumHelper
                        .getFromLength(totalLength, digitalQuality)
                }
        }
    }
}