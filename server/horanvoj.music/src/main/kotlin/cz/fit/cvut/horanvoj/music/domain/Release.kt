package cz.fit.cvut.horanvoj.music.domain

import org.hibernate.Hibernate
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Release(
    @Id
    @GeneratedValue
    override val id: Long,
    val date: LocalDateTime,
    val type: ReleaseType,
    val name: String?,
    @ManyToOne
    val album: Album,
) : DomainEntity<Long> {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Release

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return super.toString()
    }
}
