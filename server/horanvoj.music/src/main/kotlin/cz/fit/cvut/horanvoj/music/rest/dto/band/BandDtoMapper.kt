package cz.fit.cvut.horanvoj.music.rest.dto.band

import cz.fit.cvut.horanvoj.music.common.extension.orNoId
import cz.fit.cvut.horanvoj.music.domain.Band
import cz.fit.cvut.horanvoj.music.rest.dto.DtoMapper
import cz.fit.cvut.horanvoj.music.rest.dto.album.AlbumDtoMapper
import org.springframework.stereotype.Component

@Component
class BandDtoMapper(
    private val albumDtoMapper: AlbumDtoMapper,
) : DtoMapper<Band, BandDto, Long> {
    override fun Band.toDto() = BandDto(
        id = id,
        name = name,
        creationDate = creationDate,
        endDate = endDate,
        bio = bio,
        albums = with(albumDtoMapper) { albums.map { it.toDto() } },
        imageUrl = imageUrl,
    )

    override fun BandDto.toDomain() = Band(
        id = id.orNoId(),
        name = name,
        creationDate = creationDate,
        endDate = endDate,
        bio = bio,
        albums = with(albumDtoMapper) { albums.map { it.toDomain() } },
        imageUrl = imageUrl,
    )
}